function FindPictures()
%finds the times when the robot is taking a picture

global robot varnames

clear a t track amean da


still=5;
range=10;

for i=2:7
    a(i-1,:)=robot.(varnames{i}).angle;
    t(i-1,:)=robot.time;
    track(i-1,:)=robot.(varnames{i}).tracked;
end

stillframes=0;
for f=range+1:robot.framecount-range
    da=a(:,f+range)-a(:,f-range);
    dt=t(:,f+range)-t(:,f-range);
    
    if abs(da./dt)<still
        stillframes=stillframes+1;
        robot.end.pic(f)=true;
    end
end
robot.end.pic(robot.framecount)=0;

AnalizePictures()

end


function AnalizePictures()
global robot 
%find which pic frames are the same and average the data for each picture

pic=struct();

%find all still frames
p=find(robot.end.pic);
%find the number of frames between each sucessive still frame
dpic=p(2:length(p))-p(1:length(p)-1);
%find all substantially long gaps in still frames (long gaps of moving)
dpic=[find(dpic>10)];

n=0;
for i=2:length(dpic)
    %if the still frames were long enough to probably be a picture
    if (dpic(i)-dpic(i-1))>10
        n=n+1;
        %find the frames where the picture was taken
        frames=p((dpic(i-1)+1):dpic(i));
        %frame in center of picture (least likely to be wrong)
        frame=round(mean(frames));
        %update a struct with information about that picture
        pic(n).frames=frames;
        pic(n).time=(robot.time(frames));
        pic(n).endLoc=mean([robot.end.x(frames);robot.end.y(frames);robot.end.z(frames)]');
        pic(n).endA=[robot.end.Ro(:,:,frame),pic(n).endLoc';0,0,0,1];
        pic(n).baseLoc=mean([robot.end.x(frames);robot.end.y(frames);robot.end.z(frames)]');
        pic(n).baseA=robot.base.Ro(:,:,frame);
        
    end
end

robot.pic=pic;

end
