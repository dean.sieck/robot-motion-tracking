function [containers] = SetContainers()
%enables user to create a table of container locations

fprintf( 'Connecting to Natnet Client\n' )
nnc = natnet;
nnc.HostIP = '127.0.0.1';
nnc.ClientIP = '127.0.0.1';
nnc.ConnectionType = 'Multicast';

% connect the client to the server (multicast over local loopback) -
% modify for your network
while nnc.IsConnected == 0
    nnc.connect;
    if ( nnc.IsConnected == 0 )
        fprintf( 'Client failed to connect\n' )
        fprintf( '\tMake sure the host is connected to the network\n' )
        fprintf( '\tand that the host and client IP addresses are correct\n\n' )
    end
end

%find container and base location
i=1;
f=nnc.getFrame;
while ~isempty(f.RigidBody(i))
    if f.RigidBody(i).ID==9
        cID=i;
    elseif f.RigidBody(i).ID==1
        bID=i;
    end
    i=i+1;
end

fprintf(' 0 - Create New Container List \n 1 - Update Individual Container Locations \n 2 - Use Previously Saved Locations\n')
s=input ('');
if isempty(s)
    s=2;
end

clc
switch (s)
    case 0 % Update Entire List
        a=1;
        clc
        disp("make sure container is rigid body ID 9")
        disp("press enter to quit")
        fprintf("\n\n Location: %d \n",a);
        n=input('Container: ');
        
        while ~isempty(n)
            %get data for container and base
            c=nnc.getFrame.RigidBody(cID);
            
            containers(a,:)=[c.x,-c.z,c.y,n];

            a=a+1;
            clc
            disp("make sure container is rigid body ID 9")
            disp("press enter to quit")
            fprintf("\n\n Location: %d \n",a);
            n=input('Container: ');
        end
    case 1 % Update Part of List
        containers=csvread('saved files\containers.csv');
        clc
        disp("make sure container is rigid body ID 9")
        disp("press enter to quit")
        a=input('Location: ');
        
        while ~isempty(a)
            n=input('Container: ');
            
            %get data for container and base
            c=nnc.getFrame.RigidBody(cID);
            
            containers(a,:)=[c.x,-c.z,c.y,n];
            
            clc
            disp("make sure container is rigid body ID 9")
            disp("press enter to quit")
            a=input('Location: ');
        end
    case 2
        containers=csvread('saved files\containers.csv');
end


[a,q]=size(containers);

input('place robot in desired location')
b=nnc.getFrame.RigidBody(bID);
for i=1:a
    %find location of container relative to base
    qx=double( b.qx );
    qy=-double( b.qz );
    qz=double( b.qy );
    qw=double( b.qw );
    Ab=[2*(qw^2+qx^2)-1,  2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy),  b.x;...
        2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,    2*(qy*qz-qw*qx),  -b.z;...
        2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1,  b.y;...
        0,                0,                  0,                1];
    cb=Ab\[containers(i,1),containers(i,2),containers(i,3),1]';
    containerstobase(i,:)=[cb(1:3)',containers(i,4)];
end

csvwrite('saved files\containers.csv',containers);
csvwrite('saved files\containerstobase.csv',containerstobase);
disp ('containers files updated')

if exist('F:')
    csvwrite('F:\containerstobase.csv',containerstobase);
    disp ('containerstobase saved to portable drive')
end


input('');
clc
end
