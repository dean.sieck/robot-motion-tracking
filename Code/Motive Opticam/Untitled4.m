% fix robot structs

for name=["RS1","RS2","RS3","RS4","RS5","RSbroken6"]
    
    load ((name))
    
    robot.cnum=robot.containers;
    robot.containers=robot.containerLoc;
    robot=rmfield(robot,'rbnum');
    robot=rmfield(robot,'containerLoc');
    for i=8:length(varnames)
        robot=rmfield(robot,varnames{i});
    end
    
    
    save(cellstr(['saved files\',cellstr(name)]),'robot','varnames');
    
end
