function [ ] = stopTool()
%moveToLocation This function moves the arm to a location defined by the
%parameters x, y, z (in meters) and rx, ry, rz (in radians).
    global robot
    fprintf(robot, 'stopj(%.2f)\n', 0.4);
    pause(0.5)
end

