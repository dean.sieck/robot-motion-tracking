
% Optitrack Matlab / NatNet Event Handler Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later
%   - Matlab R2013

% This connects to the server and runs one of several programs showing
% robot location data

function MotiveRecording

% Set variables & include needed folders (commented out when using RunRobotMotionCapture)

global names ID robot frame varnames closeEnd
frame=1;

% create an instance of the natnet client class
fprintf( 'Connecting to Natnet Client\n' )
natnetclient = natnet;
natnetclient.HostIP = '127.0.0.1';
natnetclient.ClientIP = '127.0.0.1';
natnetclient.ConnectionType = 'Multicast';

% connect the client to the server (multicast over local loopback) -
% modify for your network
while natnetclient.IsConnected == 0
    natnetclient.connect;
    if ( natnetclient.IsConnected == 0 )
        fprintf( 'Client failed to connect\n' )
        fprintf( '\tMake sure the host is connected to the network\n' )
        fprintf( '\tand that the host and client IP addresses are correct\n\n' )
    end
end

%update lists to match motive
rbnum=0;
while ~isempty(natnetclient.getFrame.RigidBody(rbnum+1))
    rbnum=rbnum+1;
    rbID=natnetclient.getFrame.RigidBody(rbnum).ID;
    % make joint locations match with the rigid body number
    ID(rbID)=rbnum;
end

clc
disp('Connected')

%Create fields in struct for the name of each variable
%and for various constants associated with the joint

%constant distance and vector between joints > joint (i) to joint (i-1) <
p1 = [   0   -0.0028    0.4247    0.3941    0.0029   -0.0028   -0.0008;...
    0   -0.0006    0.1392   -0.1188    0.1277    0.0086    0.2722;...
    0   -0.1634    0.0101    0.0054    0.0027    0.1063    0.0101;...
    0    1.0000    1.0000    1.0000    1.0000    1.0000    1.0000];
d1=sqrt(p1(1,:).^2+p1(2,:).^2+p1(3,:).^2);

p2 = [  0.0007         0         0    0.0013    0.0024   -0.0008         0;...
    0.0004         0         0   -0.1278    0.0015   -0.2708         0;...
    0.1634         0         0   -0.0007   -0.1067    0.0294         0;...
    1.0000         0         0    1.0000    1.0000    1.0000         0];
d2=sqrt(p2(1,:).^2+p2(2,:).^2+p2(3,:).^2);
d2(2:3)=d1(3:4);

if closeEnd
    p1(:,7)=p1(:,7)*(dist(7)-.17)/(dist(7))
    d1(7)=d1(7)-.17;
    p2(:,7)=p2(:,7)*(dist(7)-.17)/(dist(7))
    d2(7)=d2(7)-.17;
end

for i=1:7
    robot.(varnames{i}).name=names(i);
    robot.(varnames{i}).d1=d1(i);
    robot.(varnames{i}).p1=p1(:,i);
    robot.(varnames{i}).d2=d2(i);
    robot.(varnames{i}).p2=p2(:,i);
end


%start recording
natnetclient.addlistener( 1 , 'recordstreaming');

input('\n\nPress Enter to Start Recording')
tic %start timer
natnetclient.enable(0)

% run Program until stopped
input('\n\nPress Enter to Stop Recording')
natnetclient.disable(0)

%Run File Analysis Programs

disp('Analizing data...')
%works best when ran at least twice (find a way to check if it is good enough and repeat later)
RobotJointAngle;
RobotJointAngle;

FindPictures

end


