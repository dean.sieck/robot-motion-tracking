function EndLocationPlot
global data time base joint1 joint3 joint4 joint5 joint6 endl enda containers numberofcontainers frames    
% plot end effector location over time        
        figure ()
        
        plot3(endl(:,1),endl(:,2),endl(:,3))
        title('Location Path of Robot Head')
        xlim([min(endl(:,1)),max(endl(:,1))])
        xlabel('m')
        ylim([min(endl(:,2)),max(endl(:,2))])
        ylabel('m')
        zlim([min(endl(:,3)),max(endl(:,3))])
        zlabel('m')
        
        
        figure ('name','End Effector Location Over Time')
        
        subplot(1,3,1)
        plot(time,endl(:,1))
        title('X Location')
        ylim([min(endl(:,1)),max(endl(:,1))])
        xlabel('Time (s)')
        ylabel('Distance (m)')
        
        subplot(1,3,2)
        plot(time,endl(:,2))
        title('Y Location')
        ylim([min(endl(:,2)),max(endl(:,2))])
        xlabel('Time (s)')
        ylabel('Distance (m)')
        
        subplot(1,3,3)
        plot(time,endl(:,3))
        title('Z Location')
        ylim([min(endl(:,3)),max(endl(:,3))])
        xlabel('Time (s)')
        ylabel('Distance (m)')
        
        
        figure ('name','End Effector Rotation Over Time')
        
        subplot(1,3,1)
        plot(time,enda(:,1))
        title('X Rotation')
        ylim([min(enda(:,1)),max(enda(:,1))])
        xlabel('Time (s)')
        ylabel('Distance (m)')
        
        subplot(1,3,2)
        plot(time,enda(:,2))
        title('Y Rotation')
        ylim([min(enda(:,2)),max(enda(:,2))])
        xlabel('Time (s)')
        ylabel('Distance (m)')
        
        subplot(1,3,3)
        plot(time,enda(:,3))
        title('Z Rotation')
        ylim([min(enda(:,3)),max(enda(:,3))])
        xlabel('Time (s)')
        ylabel('Distance (m)')
        
        
        figure()
        
        distance=sqrt(endl(:,1).^2+endl(:,2).^2+endl(:,3).^2);
        plot(time,distance(:,1))
        title('Distance From End Effector to Base')
        ylim([min(distance),max(distance)])
        xlabel('Time (s)')
        ylabel('Distance (m)')
end

