function RobotJointPositionCalibration
global names varnames robot closeEnd
disp('Create Rigid Bodies with the following IDs:')
fprintf('\n1 - Base \n2 - Joint 1 \n3 - Joint 3 \n4 - Joint 4 \n5 - Joint 5 \n6 - Joint 6  \n7 - End Effector\n8 - Bolts of base\n\n\n')
fprintf('Place origin on cart corner with offset of 10\n\n')
fprintf('Assign each robot joint angle = 0 and Reset each Rigid Body to current orientation \n\n\n')
input('Press enter to continue')
clc
repeat=1;
while repeat==1

MotiveCalibrationRecording;

clc
%adjust robot base
b(1)=robot.c1.x;
b(2)=0;
b(3)=robot.c1.z;
ba=b-[robot.base.x, robot.base.y, robot.base.z];

%adjust end effector  
clc
e=[robot.end.x, robot.end.y,robot.end.z];
%adjust for unlevel surfaces
input('Rotate Wrist 3 to -180')
MotiveCalibrationRecording;
endl=[robot.end.x, robot.end.y,robot.end.z];
e=mean([e;endl]);

%adjust joint 6  
clc
j6=[robot.j6.x,e(2),robot.j6.z];
%adjust for unlevel surfaces
input('Rotate Wrist 2 to 180')
MotiveCalibrationRecording;
a=[robot.j6.x,e(2),robot.j6.z];
j6=mean([j6;a]);
j6a=j6-[robot.j6.x, robot.j6.y, robot.j6.z];
j6a=j6a.*[-1,1,-1];

%adjust joint 5  
clc
j5(1)=robot.j5.x;
j5(2)=robot.j5.y;
j5(3)=j6(3);
%adjust for unlevel surfaces
input('Rotate Wrist 1 to -180')
MotiveCalibrationRecording;
a(1)=robot.j5.x;
a(2)=robot.j5.y;
a(3)=j6(3);
j5=mean([j5;a]);
j5a=j5-[robot.j5.x, robot.j5.y, robot.j5.z];
j5a=j5a.*[-1,-1,1];

%readjust end effector
if closeEnd
    e(3)=j6(3)+.0996;
else
    e(3)=j6(3)+.0996+.17;
end
ea=e-endl;
ea=ea.*[-1,-1,1];

%Adjust joint 1
j1(1)=b(1);
j1(2)=b(2)+.1625;
j1(3)=b(3);
j1a=j1-[robot.j1.x,robot.j1.y,robot.j1.z];

%Adjust joint 3
j3(1)=j1(1)-.425;
j3(2)=mean([j5(2),j1(2)]); 
j3(3)=j1(3)+.138;
j3a=j3-[robot.j3.x,robot.j3.y,robot.j3.z];

%Adjust joint 4
j4(1)=j5(1);
j4(2)=j5(2);
j4(3)=j5(3)-.131;
j4a=j4-[robot.j4.x,robot.j4.y,robot.j4.z];


clc
disp("Adjust Robot Base") %to match bottom center of base
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',ba)
input('');
disp("Adjust Joint 1") 
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',j1a)
input('');
disp("Adjust Joint 3") 
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',j3a)
input('');
disp("Adjust Joint 4") 
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',j4a)
input('');
disp("Adjust Joint 5") 
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',j5a)
input('');
disp("Adjust Joint 6") 
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',j6a)
input('');
disp("Adjust End Effector") 
fprintf('  X = %f \n  Y = %f \n  Z = %f \n',ea)
input('');

%verify

input('Place Robot back in 0 position to verify');
MotiveCalibrationRecording
err(1,:)=b-[robot.base.x, robot.base.y, robot.base.z];
err(2,:)=j1-[robot.j1.x, robot.j1.y, robot.j1.z];
err(3,:)=j3-[robot.j3.x, robot.j3.y, robot.j3.z];
err(4,:)=j4-[robot.j4.x, robot.j4.y, robot.j4.z];
err(5,:)=j5-[robot.j5.x, robot.j5.y, robot.j5.z];
err(6,:)=j6-[robot.j6.x, robot.j6.y, robot.j6.z];
err(7,:)=e-[robot.end.x, robot.end.y, robot.end.z];
clc
fprintf('\nMax Error: %f\n\n',max(max(err)))
err=[names(1:7)',err];
fprintf('%s  X= %s Y= %s Z= %s\n',err')
clear err
repeat=input('enter 1 to repeat ');
end
end


function MotiveCalibrationRecording

%% Set variables & include needed folders (commented out when using RunRobotMotionCapture)

global names ID robot frame varnames
frame=1;


%addpath('event handlers')
%addpath('sub programs')
%addpath('..\saved files')

%robot=struct();
%names=["Robot Base","Joint 1","Joint 3","Joint 4","Joint 5","Joint 6","End Effector"];
% respective name ID numbers: [1,2,3,4,5,6,7];
varnames={'base','j1','j3','j4','j5','j6','end'};


%% create an instance of the natnet client class
fprintf( 'Connecting to Natnet Client\n' )
natnetclient = natnet;
natnetclient.HostIP = '127.0.0.1';
natnetclient.ClientIP = '127.0.0.1';
natnetclient.ConnectionType = 'Multicast';

% connect the client to the server (multicast over local loopback) -
% modify for your network
while natnetclient.IsConnected == 0
    natnetclient.connect;
    if ( natnetclient.IsConnected == 0 )
        fprintf( 'Client failed to connect\n' )
        fprintf( '\tMake sure the host is connected to the network\n' )
        fprintf( '\tand that the host and client IP addresses are correct\n\n' )
    end
end

%update lists to match motive
rbnum=0;
while ~isempty(natnetclient.getFrame.RigidBody(rbnum+1))
   rbnum=rbnum+1;
   rbID=natnetclient.getFrame.RigidBody(rbnum).ID;
    % make joint locations match with the rigid body number
    ID(rbID)=rbnum;
    %add names for containers
    if rbID>7
        names=[names,sprintf('Container %d',rbID-7)]; 
        varnames=[varnames,sprintf('c%i',rbID-7)];
    end
end


%% Set up recording
clc
disp('Collecting Data...')

robot.rbnum=rbnum;
robot.containers=rbnum-7;

%Create fields in struct for the name of each variable
for i=1:rbnum
    robot.(varnames{i}).name=names(i);
end

natnetclient.addlistener( 1 , 'recordstreaming');

tic %start timer

%% Start recording
natnetclient.enable(0)

% run Program until stopped
pause (2)
natnetclient.disable(0)

for i=1:8
    x=robot.(varnames{i}).x;
    y=robot.(varnames{i}).z;
    z=-robot.(varnames{i}).y;
    robot.(varnames{i}).x=mean(x);
    robot.(varnames{i}).y=mean(y);
    robot.(varnames{i}).z=mean(z);
end



end
