
function RobotSketch
%Draws a sketch of the robot as it moved

global robot varnames range p sketch
range=1.5;

%setup plot
figure ('Name','Robot Sketch','Position',[1921 177 1280 948])


%initialize line for sketch
sketch=plot3((1:7),(1:7),(1:7),'r');
hold on
p=plot3(0,0,0);
%initialize rigid body markers
p(1)=plot3(0,0,0,'Marker','s','Color','r','MarkerSize',10);%base
for i=2:6
    p(i)=plot3(0,0,0,'Marker','d','Color','r'); %Joints
end
p(7)=plot3(0,0,0,'Marker','p','Color','r','MarkerSize',10); %end Effector
for i=1:robot.cnum
    p(i+7)=plot3(0,0,0,'Marker','o','Color','b','MarkerFaceColor','c');%containers
end
axis([-1,1,-1,1,-1,1]);
xlabel('x');ylabel('y');zlabel('z');

time=0;
while isempty(time) || time~=-1;
    if isempty(time)
        clc
        speed=input('Enter playback speed: ');
        if speed>=1
            s=round(speed*5);
        else
            s=1;
        end
        for j=s+1:s:robot.framecount
            tic
            gototime(robot.time(j));
            time((j-1)/s)=(robot.time(j)-robot.time(j-s))/(speed)-toc;
            pause(time((j-1)/s));
        end
    else
        gototime(time)
    end
    clc
    disp('Enter -1 to quit')
    disp('Press enter to play entire streaming')
    time=input('Go to time:');
    
end



end

function gototime(time)
global robot varnames range p sketch
for j=1:robot.framecount
    if time<robot.time(j)
        break
    end
end
for i=1:7 %for all joints
    %make arrays for line
    x(i)=robot.(varnames{i}).x(j);
    y(i)=robot.(varnames{i}).y(j);
    z(i)=robot.(varnames{i}).z(j);
    
    %update rigid body markers
    p(i).XData=x(i);
    p(i).YData=y(i);
    p(i).ZData=z(i);
    if robot.(varnames{i}).tracked(j)==true
        p(i).MarkerFaceColor='r';
    else
        p(i).MarkerFaceColor='w';
    end
end

if robot.end.pic(j)
    p(7).Color='b';
else
    p(7).Color='r';
end

for i=1:robot.cnum %for all containers
    p(i+7).XData=robot.containers(i,1);
    p(i+7).YData=robot.containers(i,2);
    p(i+7).ZData=robot.containers(i,3);
end
%update line
sketch.XData=x;
sketch.YData=y;
sketch.ZData=z;

title(sprintf('Time: %.01f s',robot.time(j)));
axis([robot.base.x(j)-range,robot.base.x(j)+range,robot.base.y(j)-range,robot.base.y(j)+range,robot.base.z(j)-range,robot.base.z(j)+range]);
end




