function [ ] = moveJointsTimeJ(J1, J2, J3, J4, J5, J6, t)
%moveToLocation This function moves the arm to a location defined by the
%parameters x, y, z (in meters) and rx, ry, rz (in radians).
global robot    
    A = [J1, J2, J3, J4, J5, J6]*0.0174533;
    A(7) = t;
    fprintf(robot, 'stopj(0.3)\n');
    pause(0.3)
    fprintf(robot, 'movej([%.3f, %.3f, %.3f, %.4f, %.4f, %.4f], a=.1,v=.1,t=%.3f)\n',A);
end

