%By Steven Lukow

%Housekeeping
clc
clear all
close all


shapeLoc = [0..927, -.053,.426]; %Relative to robot base
shapeRadius = 0.0825;
cameraShiftAngle =15; %Degrees
%cameraDistance = 0.3; %based on FOV of cameras
tableX = [-.17 - 0.15, .435 + 0.15];
tableY = [-1,.12+0.15];

startingAngle = atan2d(shapeLoc(2),shapeLoc(1)) + 180;

fill3([-.17, .435, .435, -0.17],[.12, .12, -0.93, -0.93],[0,0,0,0], [.5, .5, .5])
circle = linspace(-180,+180,360/cameraShiftAngle);
%zPoints = max(0.16, shapeLoc(3)-0.03):.1:shapeLoc(3)+0.3;
zPoints = shapeLoc(3)-0.03:.1:shapeLoc(3)+0.3;
circleIndex = [];


%Define distance from camera end effector to cylinder wall
cameraD = 0.3;

%Link Dimensions
wristD = 0.09960141; %wristD = 0.0823;
toolD = 0.170;
L1 = .42499948; %L1 = 0.4257;
L2 = 0.39214349;  %L2 = 0.39155;

%Combination lengths of tool and camera distance
comboD = wristD + toolD + cameraD;
camTool = toolD + cameraD;
baseSideD = 0.13779926- 0.0059539; %baseSideD = 0.0997;

if sqrt(shapeLoc(1)^2 + shapeLoc(2)^2) < 0.8% || (sqrt(shapeLoc(1)^2 + shapeLoc(2)^2) < 1 && shapeLoc(3) < 0.1)
    fprintf('Container too close!  Move robot into a better position.\n')
    return;
end

figure(1)
hold on
inc = 1;
jointInc = 1;
wristUp = 0;
for z = zPoints
    %plot3(cosd(circle)*0.1 + shapeLoc(1), sind(circle)*0.1 + shapeLoc(2), zeros(size(circle)) + z, 'b*', 'HandleVisibility', 'off')
    circle = -circle;
    for desiredYaw = circle + startingAngle%linspace(-pi/2, -pi/2 + 2*pi, numPoints+1)%[linspace(-boxSize, boxSize, numPoints), ones(1,numPoints+1)*boxSize, linspace(boxSize, -boxSize, numPoints), ones(1,numPoints+1)*-boxSize]
        possible= 1;
        tableBump = 0;
        %inc = inc + 1;
        %Define default configuration
        
        
        %Calculate first link location and rotation
        desiredPitch = 0;
        %z = sind(desiredPitch)*comboD;
        rotateD = cosd(desiredPitch)*comboD;
        rotateD2 = cosd(desiredPitch)*camTool;
        cameraWrist = [shapeLoc(1) + rotateD*cosd(desiredYaw), shapeLoc(2) + rotateD*sind(desiredYaw),z];
        
        %IK plane target point
        desiredPoint1 = [shapeLoc(1) + rotateD*cosd(desiredYaw), shapeLoc(2) + rotateD*sind(desiredYaw), z + 0.101];
        desiredPoint2 = [shapeLoc(1) + rotateD*cosd(desiredYaw), shapeLoc(2) + rotateD*sind(desiredYaw),  z - 0.101];
        testAgain = 1;
        
        wristUp(inc) = 1;
        desiredPoint = desiredPoint1;
        theta1 = -1000;
        
        %Find useful lengths for RRR IK
        xyD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2);
        xyzD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2 + desiredPoint(3)^2);
        linkD = sqrt(xyD^2 -(baseSideD)^2);
        %while (tableBump == 1)||(theta1 == -1000)
        %    tableBump = 0;
        if isreal(linkD)
            %Work out RRR plane orientation
            x = desiredPoint(1);
            y = desiredPoint(2);
            d = baseSideD;
            if (x<tableX(2))&&(y<tableY(2))&&(x>tableX(1))&&(desiredPoint(3)<.2)%||((y<-.20)&&(desiredPoint(3)<0.3)&&(x>0))
                %fprintf('Hit Table1\n')
                desiredPoint(3) = 0.22;
                tableBump = 1;
                %possible = 0;
            end
            %Find points tangent to base circluar motion
            a1 = (2*(d^2)*y + sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
            a2 = (2*(d^2)*y - sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
            b1 = sqrt((d^2)-(a1^2));
            b2 = sqrt((d^2)-(a2^2));
            if desiredPoint(1) >= 0 && desiredPoint(2) >= -baseSideD
                bTheta = atan2d(a2,b2);
            elseif desiredPoint(1) >= 0 && desiredPoint(2) <= baseSideD
                bTheta = -(atan2d(a2,b2)+180);
            elseif desiredPoint(1) <= 0 && desiredPoint(2) <= baseSideD
                bTheta = 180-((atan2d(a1,b1)));
            else
                bTheta = (atan2d(a1,b1));
            end
            
            %Find starting and ending point of RRR
            sP = [baseSideD*cosd(bTheta),baseSideD*sind(bTheta),0.1625];
            dZ = desiredPoint(3)-sP(3);
            
            %Two main joints
            theta2 = -acosd((linkD^2 + dZ^2 - L1^2 - L2^2)/(2*L1*L2));
            if ~isreal(theta2) || ~isreal((L2*sind(theta2))) || ~isreal((L1 + L2*cosd(theta2)))
                %fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                possible = 0;
            else
                theta1 = (atan2d(dZ,linkD)-atan2d((L2*sind(theta2)),(L1 + L2*cosd(theta2))));
                if ~isreal(theta1)
                    %fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                    possible = 0;
                else
                    if (L1*sind(theta1) <= -.1)
                        %fprintf('Hit Table2\n')
                        desiredPoint(3) = 0.22;
                        tableBump = 1;
                        xyzD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2 + desiredPoint(3)^2);
                        linkD = sqrt(xyD^2 -(baseSideD)^2);
                        if isreal(linkD)
                            a1 = (2*(d^2)*y + sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
                            a2 = (2*(d^2)*y - sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
                            b1 = sqrt((d^2)-(a1^2));
                            b2 = sqrt((d^2)-(a2^2));
                            if desiredPoint(1) >= 0 && desiredPoint(2) >= -baseSideD
                                bTheta = atan2d(a2,b2);
                            elseif desiredPoint(1) >= 0 && desiredPoint(2) <= baseSideD
                                bTheta = -(atan2d(a2,b2)+180);
                            elseif desiredPoint(1) <= 0 && desiredPoint(2) <= baseSideD
                                bTheta = 180-((atan2d(a1,b1)));
                            else
                                bTheta = (atan2d(a1,b1));
                            end
                            
                            %Find starting and ending point of RRR
                            sP = [baseSideD*cosd(bTheta),baseSideD*sind(bTheta),0.1625];
                            dZ = desiredPoint(3)-sP(3);
                            
                            %Two main joints
                            theta2 = -acosd((linkD^2 + dZ^2 - L1^2 - L2^2)/(2*L1*L2));
                            if ~isreal(theta2) || ~isreal((L2*sind(theta2))) || ~isreal((L1 + L2*cosd(theta2)))
                                %fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                                possible = 0;
                            else
                                theta1 = (atan2d(dZ,linkD)-atan2d((L2*sind(theta2)),(L1 + L2*cosd(theta2))));
                                if ~isreal(theta1)
                                    %fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                                    possible = 0;
                                else
                                    if ((theta1 + theta2) > 20)
                                        desiredPoint = desiredPoint2;
                                        fprintf('Should flip?\n')
                                        testAgain = 1;
                                    else
                                        testAgain = 0;
                                    end
                                end
                            end
                        else
                            possible = 0;
                        end
                    end
                    if ((theta1 + theta2) > 20)
                        desiredPoint = desiredPoint2;
                        fprintf('Should flip?\n')
                        testAgain = 1;
                    else
                        testAgain = 0;
                    end
                end
            end
        else
            possible = 0;
        end
        
        
        if possible == 0 || testAgain == 1
            possible = 1;
            desiredPoint = desiredPoint2;
            
            %Find useful lengths for RRR IK
            xyD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2);
            xyzD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2 + desiredPoint(3)^2);
            linkD = sqrt(xyD^2 -(baseSideD)^2);
            if isreal(linkD)
                %Work out RRR plane orientation
                x = desiredPoint(1);
                y = desiredPoint(2);
                d = baseSideD;
                
                %Find points tangent to base circluar motion
                a1 = (2*(d^2)*y + sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
                a2 = (2*(d^2)*y - sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
                b1 = sqrt((d^2)-(a1^2));
                b2 = sqrt((d^2)-(a2^2));
                if desiredPoint(1) >= 0 && desiredPoint(2) >= -baseSideD
                    bTheta = atan2d(a2,b2);
                elseif desiredPoint(1) >= 0 && desiredPoint(2) <= baseSideD
                    bTheta = -(atan2d(a2,b2)+180);
                elseif desiredPoint(1) <= 0 && desiredPoint(2) <= baseSideD
                    bTheta = 180-((atan2d(a1,b1)));
                else
                    bTheta = (atan2d(a1,b1));
                end
                
                %Find starting and ending point of RRR
                sP = [baseSideD*cosd(bTheta),baseSideD*sind(bTheta),0.1625];
                dZ = desiredPoint(3)-sP(3);
                
                %Two main joints
                theta2 = -acosd((linkD^2 + dZ^2 - L1^2 - L2^2)/(2*L1*L2));
                if ~isreal(theta2) || ~isreal((L2*sind(theta2))) || ~isreal((L1 + L2*cosd(theta2)))
                    %fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                    possible = 0;
                else
                    theta1 = (atan2d(dZ,linkD)-atan2d((L2*sind(theta2)),(L1 + L2*cosd(theta2))));
                    if ~isreal(theta1)
                        %fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                        possible = 0;
                    else
                        if tableBump == 1%(x<tableX(2))&&(y<tableY(2))&&(x>tableX(1))&&(desiredPoint(3)<.2)||(L1*sind(theta1) <= 0.2)||((y<-0)&&(desiredPoint(3)<0.3))
                            %desiredPoint(3) = 0.2;
                            possible = 0;
                        end
                        
                    end
                end
                
            else
                possible = 0;
            end
            wristUp(inc) = 0;
            
        end
        
        
        %plot3([0,desiredPoint(1)],[0,desiredPoint(2)], [0,desiredPoint(3)], '*', 'color', 'c')
        if possible == 1
            
            %clf
            hold on
            pbaspect([1 1 1])
            %plot3(cosd(desiredYaw)*shapeRadius + shapeLoc(1), sind(desiredYaw)*shapeRadius + shapeLoc(2), z, 'ro', 'HandleVisibility', 'off')
            
            plot3([0,0],[0,0], [0,sP(3)], 'linewidth', 3, 'color', 'b')
            plot3([0,sP(1)],[0,sP(2)], [sP(3),sP(3)], 'b', 'linewidth', 2.5)
            
            plot3([sP(1), sP(1)+L1*cosd(theta1)*cosd(bTheta+90)],[sP(2), sP(2)+L1*cosd(theta1)*sind(bTheta+90)],[sP(3), L1*sind(theta1) + sP(3)], 'b', 'linewidth', 2)
            elbow = [L1*cosd(theta1)*cosd(bTheta+90), L1*cosd(theta1)*sind(bTheta+90), L1*sind(theta1) + sP(3)];
            wrist = [L2*cosd(theta2+theta1)*cosd(bTheta+90), L2*cosd(theta2+theta1)*sind(bTheta+90), L2*sind(theta2+theta1)];
            plot3([sP(1)+L1*cosd(theta1)*cosd(bTheta+90), elbow(1)],[sP(2)+L1*cosd(theta1)*sind(bTheta+90), elbow(2)],[L1*sind(theta1) + sP(3), elbow(3)], 'b', 'linewidth', 1.5)
            
            plot3([elbow(1), elbow(1) + wrist(1)],[elbow(2), elbow(2) + wrist(2)],[elbow(3), elbow(3) + wrist(3)],'b')
            plot3([elbow(1) + wrist(1), elbow(1) + wrist(1) + baseSideD*cosd(bTheta)],[ elbow(2) + wrist(2), elbow(2) + wrist(2) + baseSideD*sind(bTheta)],[elbow(3) + wrist(3), elbow(3) + wrist(3)],'b', 'linewidth', 1.2)
            
            plot3([shapeLoc(1), cameraWrist(1)],[shapeLoc(2), cameraWrist(2)],[z, cameraWrist(3)], 'c:', 'linewidth', 1)
            if wristUp(inc) == 1
                plot3([cameraWrist(1), cameraWrist(1)],[cameraWrist(2), cameraWrist(2)],[cameraWrist(3), cameraWrist(3)+0.0997], 'c')
            else
                plot3([cameraWrist(1), cameraWrist(1)],[cameraWrist(2), cameraWrist(2)],[cameraWrist(3), cameraWrist(3)-0.0997], 'c')
            end
            
            
            [cX, cY, cZ] = cylinder();
            surf(cX*shapeRadius+shapeLoc(1), cY*shapeRadius+shapeLoc(2), cZ*.2032 + shapeLoc(3), 'HandleVisibility', 'off')
            %surf(cX*0.064, cY*0.064, cZ*0.086)
            %surf(cX, cY, cZ)
            %surf(sX*.05, sY*.05, sZ*.05 + 0.086)
            %if jointInc == 1
            % view(20, 40)
            %end
            axis([-1,1,-1,1, -.5, 1.5])
            xlabel('X')
            ylabel('Y')
            pause(0.02)
            
            %Write joint angles for the robot to use
            J1(jointInc) = bTheta -90;%-360;
            J2(jointInc) = theta1-180;
            J3(jointInc) = theta2;
            J4(jointInc) = abs(J2(jointInc)) + abs(J3(jointInc))-360;
            J5(jointInc) =  J1(jointInc) - desiredYaw + 90+180;
            if J5(jointInc) > 360
                J5(jointInc) =  J5(jointInc) - 360;
            end
            if J5(jointInc) >=90
                J5(jointInc) = J5(jointInc)-360;
            end
            if J5(jointInc) < -200
                J5(jointInc) = J5(jointInc) + 360;
            end
            T(jointInc) = 3;
            J6(jointInc) = 45;
            if wristUp(inc) == 0
                T(jointInc) = 3;
                J1(jointInc) = bTheta-90;
                J4(jointInc) =J4(jointInc) + 180;
                J5(jointInc) =(desiredYaw-180)-bTheta;
                if J5(jointInc) < -360
                    J5(jointInc) = J5(jointInc) + 360;
                end
                if J5(jointInc) < -360
                    J5(jointInc) = J5(jointInc) + 360;
                end
                if J5(jointInc) < -100
                    J5(jointInc) = J5(jointInc) + 360;
                end
                J6(jointInc) =J6(jointInc) - 180;
            end
            jointInc = jointInc + 1;
            %fprintf('%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n', J1, J2, J3, J4, J5, J6)
        end
        inc = inc + 1;
    end
end
allJoints = [J1;J2;J3;J4;J5;J6];


%%
%Connect to robot and run motions

tempData = input('Press Enter to begin moving robot');

global robot
robot = robotSetup();
robotHome()
pause(6)
for j = 1:length(J1)
    if j == 1
        moveJointsTimeJ(J1(j), J2(j), J3(j), J4(j), J5(j), J6(j), 7);
        pause(8);
    else
        if J6(j-1) ~= J6(j)
            moveJointsTimeJ(J1(j), J2(j), J3(j), J4(j), J5(j), J6(j), 4);
            pause(4.2);
        else
           t = 1.5* cameraShiftAngle/15;
           if abs(J1(j) - J1(j-1)) > 180
               t = 10;
           end
            moveJointsTimeJ(J1(j), J2(j), J3(j), J4(j), J5(j), J6(j), t);
            pause(t+0.2);
        end
    end
end
fprintf('Finished!\n')
robotHome()


