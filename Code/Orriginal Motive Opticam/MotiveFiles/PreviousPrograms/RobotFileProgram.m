function RobotFileProgram
clc
global filename 
testdata=input ('enter 1 to use ''Test.csv'' data ');

if  testdata ==1 
    filename='Test';
    disp ('Collecting Data...')
    MotiveFileRobotAndContainers;
else
    filename=input('File Name: ','s');
    disp ('Collecting Data...')
    MotiveFileRobotAndContainers;
end
clc
repeat=1;
while repeat==1
    disp(' ')
    
    options={...
        '1-Calibrate end effector and base'...
        '2-List average joint locations'...
        '3-Plot end effector distance from containers'...
        '4-Plot end effector location over time'...
        };
    
    disp(options')
    select=input ('Enter Program Selection: ');
    clc
    
    disp(options(select))
    pause(1)
    
    switch select
        case(1)
            CalibrateEndBase
        case(2)
            JointLocations
        case (3)
            ContainersToEnd
        case (4)
            EndLocationPlot
    end
    
    fprintf('\n\n')
    repeat=input('Enter 1 to run another program ');
end

end


