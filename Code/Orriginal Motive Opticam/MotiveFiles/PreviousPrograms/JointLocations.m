function JointLocations
global data time base joint1 joint3 joint4 joint5 joint6 endl enda containers numberofcontainers frames    
   
% List average joint locations

        disp('(base relative to origin)')
        Base=mean(base)
        disp('(all others relative to base)')
        Joint1=mean(joint1)
        Joint3=mean(joint3)
        Joint4=mean(joint4)
        Joint5=mean(joint5)
        Joint6=mean(joint6)
        EndEffectorLocation=mean(endl)
        EndEfffectorAngle=mean(enda)
        
        hj1=EndEffectorLocation-Joint1;
        distanceHeadtoJoint1=sqrt(hj1(1)^2+hj1(2)^2+hj1(3)^2)
        distanceHeadtoBase=sqrt(EndEffectorLocation(1)^2+EndEffectorLocation(2)^2+EndEffectorLocation(3)^2)
   
end

