function SetupRecording()
%setup the table to record data in

global frame rec ID varnames names
frame=1;
rec=struct();

rec.rbnum=length(ID);


%create struct variable names
varnames={'base','j1','j3','j4','j5','j6','end'};
for i=1:rec.rbnum-7
    varnames=varnames+sprintf('c%i',i);
    
end

%Create fields in struct for the name of each variable
for i=1:rec.rbnum
    rec.(varnames{i}).name=names(i);
    
end

%start timer    
tic
end

