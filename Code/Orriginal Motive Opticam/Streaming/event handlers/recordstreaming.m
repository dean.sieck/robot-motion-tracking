
 function recordstreaming( ~ , evnt )
	% The event callback function executs each time a frame of mocap data is delivered.
	% to Matlab. Matlab will lag if the data rate from the Host is too high.
	% A simple animated line graphs the x, y, z position of the first rigid body on the Host.

	
	% Note - This callback uses the gobal variables px, py, pz from the NatNetEventHandlerSample.
	global robot frame varnames ID
	persistent lastframe1
    
    %mirror the check performed in the sample code for duplicate frame
    frame1 = double( evnt.data.iFrame );
	if ~isempty( frame1 ) && ~isempty( lastframe1 )
		if frame1 >= lastframe1
            frame=frame+1;
		end
	end
    
    robot.time(frame)=toc;
    robot.frame(frame)=frame;
    robot.framecount=frame;
    
    for i=1:length(ID)
        rb = evnt.data.RigidBodies( ID(i) );

        %record position with desired coordinates
        robot.(varnames{i}).x(frame)=double( rb.x);
        robot.(varnames{i}).y(frame)=double( -rb.z);   
        robot.(varnames{i}).z(frame)=double( rb.y);
        
        if i<8
            %Record Quaternion Angle
            robot.(varnames{i}).qx(frame) = double(rb.qx);
            robot.(varnames{i}).qy(frame) = -double(rb.qz);
            robot.(varnames{i}).qz(frame) = double(rb.qy);
            robot.(varnames{i}).qw(frame) = double(rb.qw);
            
            %         %convert Quaternion to euler
            %         qx = double(rb.qx);
            %         qy = double(rb.qy);
            %         qz = double(rb.qz);
            %         qw = double(rb.qw);
            %         q = quaternion( qx, qy, qz, qw );
            %         qRot = quaternion( 0, 0, 0, 1);
            %         q = mtimes( q, qRot);
            %         a = EulerAngles( q , 'zyx' );
            %         robot.(varnames{i}).ax(frame) = a( 1 );
            %         robot.(varnames{i}).ay(frame) = a( 2 );
            %         robot.(varnames{i}).az(frame) = a( 3 );
            
            %record if robot.(varnames{i}) was tracked
            if rb.MeanError ==0
                robot.(varnames{i}).tracked(frame)=false;
            else
                robot.(varnames{i}).tracked(frame)=true;
            end
        end
                
    end
        
	% Update lastframe
	lastframe1 = frame1;
    
    clc
    toc
    disp('Press Enter to Stop Recording')
end  % eventcallback1
