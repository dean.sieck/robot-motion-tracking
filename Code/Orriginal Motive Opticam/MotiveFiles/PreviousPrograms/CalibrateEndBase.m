function CalibrateEndBase
% Calibrate End Effector and Base Locations
global data time base joint1 joint3 joint4 joint5 joint6 endl enda containers numberofcontainers frames    
   
        j6=mean(joint6);
        el=mean(endl);
        a=(j6-el);
        %fprintf('\na(3)=a(3)+%0.8f\n',-a(3))  %if offset needs adjusting
        a(3)=a(3)+.34914772; 
        fprintf('\nAdjust End Effector Pivot Point \nX = %0.6f \nY = %0.6f \nZ = %0.6f\n',a(1),a(2),a(3))
        EndAngle=mean(enda);
        
        j1=mean(joint1);
        b=mean(base);
        fprintf('\nAdjust Base Pivot Point \nX = %0.6f \nY = %0.6f  \nZ = %0.6f\n',j1(1),-b(2),j1(3))
        

end

