

function CreateJointPlots
	% sets up two plots for viewing rigid body data, position and rotation
	global  jl JL
	% making animated lines global so they can be accessed in the
	% callback functions
	global px py pz
    px=animatedline();
    py=animatedline();
    pz=animatedline();
    
    names=["Joint 1","Joint 3","Joint 4","Joint 5","Joint 6","End Effector"];


    JL=figure(1);
    JL.Name='Joint Locations';
    JL.Position=[1921 177 1280 948];
    for i=1:6
        % plot an animated line for position
        jl(i) = subplot( 2,3,i);
        title( names(i) );
        xlabel( 'Time (s)' )
        ylabel( 'Position (m)' )

        px(i) = animatedline();
        px(i).Marker = '.';
        px(i).LineWidth = 0.5;
        px(i).Color = [ 1 0 0 ];

        py(i) = animatedline();
        py(i).MaximumNumPoints = 1000;
        py(i).LineWidth = 0.5;
        py(i).Color = [ 0 1 0 ];
        py(i).Marker = '.';

        pz(i) = animatedline;
        pz(i).MaximumNumPoints = 1000;
        pz(i).LineWidth = 0.5;
        pz(i).Color = [ 0 0 1 ];
        pz(i).Marker = '.';
    end

    legend({'X','Y','Z'},'Position',[0.4512 0.0218 0.1352 0.0226],'Orientation','horizontal')

end
