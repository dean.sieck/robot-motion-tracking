function Save2Table
%Saves Robot struct to table 
%   stores frame and time
%   stores tracked, x,y,x, qx,qy,qz and angle for each joint 
%   stores x,y,z for containers

global varnames robot

frame=robot.frame';
time=robot.time';
t=table(frame,time);

for i=1:7 %collect data for robot joints
    n=varnames{i};
    eval(sprintf("%s_tracked=robot.%s.tracked';",n,n))
    
    eval(sprintf("%s_x=robot.%s.x';",n,n))
    eval(sprintf("%s_y=robot.%s.y';",n,n))
    eval(sprintf("%s_z=robot.%s.z';",n,n))
    
    eval(sprintf("%s_qx=robot.%s.qx';",n,n))
    eval(sprintf("%s_qy=robot.%s.qy';",n,n))
    eval(sprintf("%s_qz=robot.%s.qz';",n,n))
    eval(sprintf("%s_qw=robot.%s.qw';",n,n))
    
    eval(sprintf("%s_angle=robot.%s.angle';",n,n))
    
    %put data into table
    eval(sprintf("t=[t,table(%s_tracked,%s_x,%s_y,%s_z,%s_qx,%s_qy,%s_qz,%s_qw,%s_angle)];",n,n,n,n,n,n,n,n,n))
end


for i=8:robot.rbnum 
    n=varnames{i};
    
    %collect location data for containers 
    eval(sprintf("%s_x=robot.%s.x';",n,n))
    eval(sprintf("%s_y=robot.%s.y';",n,n))
    eval(sprintf("%s_z=robot.%s.z';",n,n))
    
    
    eval(sprintf("t=[t,table(%s_x,%s_y,%s_z)];",n,n,n))
end

file=input('Input table file name: ','s');
if isempty(file)
    file='RobotTable';
else
    while exist(['saved files\Robot Tables\',file,'.txt'])
        clc
        disp('File name already in use')
        disp('Enter new name or press enter to overwrite')
        f=input('Input table file name: ','s');
        if isempty(f)
            break
        end
        file=f; 
    end
end

writetable(t,['saved files\Robot Tables\',file,'.txt']);
end

