
function basepositioning( ~ , evnt )

	global x z angle base ang
    global bID
  	
    
    % Get the rigid body position
    bx = double( evnt.data.RigidBodies( bID ).x );
    bz = double( evnt.data.RigidBodies( bID ).z );
    qy = evnt.data.RigidBodies( bID ).qy;
    qw = double( evnt.data.RigidBodies( bID ).qw);
    ba = acos(qw)*2*180/pi;
    if qy<0
        ba=-ba;
    end
    ax=sind(ba)/10+bx;
    az=cosd(ba)/10+bz;
    
    %Plot Position
    base.clearpoints;
	base.addpoints(bx,bz);
    ang.clearpoints;
    ang.addpoints([bx,ax],[bz,az]);
	drawnow
	
    %Print Movement
    clc
    disp('Move Robot Base:')
    if x-bx<0
        fprintf(' %0.1fcm Towards Wall\n',-(x-bx)*100);
    else 
        fprintf(' %0.1fcm Away From Wall\n',(x-bx)*100);
    end
    if z-bz<0
        fprintf(' %0.1fcm Away From Door\n\n',-(z-bz)*100);
    else 
        fprintf(' %0.1fcm Towards Door\n\n',(z-bz)*100);
    end
    
    disp('Rotate Robot Base')
    if angle-ba>0
        fprintf(' %0.1f%s Counter Clockwise\n',(angle-ba),char(176));
    else 
        fprintf(' %0.1f%s Clockwise\n',-(angle-ba),char(176));
    end
    
end  
