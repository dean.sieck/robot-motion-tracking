global robot varnames
format compact
clear r11 r12 r13 r21 r22 r23 r31 r32 r33

%plots the components of each R matrix that is not 0


for i=2:7
    figure(i-1)
    for j=1:length(robot.(varnames{i}).R)
        r11(j)=robot.(varnames{i}).R(1,1,j);
        r12(j)=robot.(varnames{i}).R(1,2,j);
        r13(j)=robot.(varnames{i}).R(1,3,j);
        
        r21(j)=robot.(varnames{i}).R(2,1,j);
        r22(j)=robot.(varnames{i}).R(2,2,j);
        r23(j)=robot.(varnames{i}).R(2,3,j);
        
        r31(j)=robot.(varnames{i}).R(3,1,j);
        r32(j)=robot.(varnames{i}).R(3,2,j);
        r33(j)=robot.(varnames{i}).R(3,3,j);
    end
    hold off
    plot(0,0)
    hold on
    
    switch i
        case 2
            plot(robot.frame,r11)
            plot(robot.frame,r12)
            plot(robot.frame,r21)
            plot(robot.frame,r22)
            plot(robot.frame,r33)
            legend('0','r11','r12','r21','r22','r33')
            
            %           plot(robot.frame,r13)
            %           plot(robot.frame,r23)
            %           plot(robot.frame,r31)
            %           plot(robot.frame,r32)
            %           legend('0','r13','r23','r31','r32')
        case {3,4,5}
            plot(robot.frame,r11)
            plot(robot.frame,r13)
            plot(robot.frame,r22)
            plot(robot.frame,r31)
            plot(robot.frame,r33)
            legend('0','r11','r13','r22','r31','r33')
            
            %             plot(robot.frame,r12)
            %             plot(robot.frame,r21)
            %             plot(robot.frame,r23)
            %             plot(robot.frame,r32)
            %           legend('0','r12','r21','r23','r32')
        case 6
            plot(robot.frame,r11)
            plot(robot.frame,r12)
            plot(robot.frame,r21)
            plot(robot.frame,r22)
            plot(robot.frame,r33)
            legend('0','r11','r12','r21','r22','r33')
            
            %             plot(robot.frame,r13)
            %             plot(robot.frame,r23)
            %             plot(robot.frame,r31)
            %             plot(robot.frame,r32)
            %           legend('0','r13','r23','r31','r32')
        case 7
            plot(robot.frame,r11)
            plot(robot.frame,r13)
            plot(robot.frame,r22)
            plot(robot.frame,r31)
            plot(robot.frame,r33)
            legend('0','r11','r13','r22','r31','r33')
            
            %             plot(robot.frame,r12)
            %             plot(robot.frame,r21)
            %             plot(robot.frame,r23)
            %             plot(robot.frame,r32)
            %           legend('0','r12','r21','r23','r32')
    end
    title(robot.(varnames{i}).name)
end

