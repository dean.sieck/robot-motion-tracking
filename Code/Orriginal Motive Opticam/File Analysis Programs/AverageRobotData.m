%average all values
function AverageRobotData(f1,f2)
global robot varnames
joint=struct;
for i=1:7
    joint(i).name=robot.(varnames{i}).name;
    joint(i).d1=robot.(varnames{i}).d1;
    joint(i).d2=robot.(varnames{i}).d2;
    joint(i).p1=robot.(varnames{i}).p1;
    joint(i).p2=robot.(varnames{i}).p2;
    joint(i).x=mean(robot.(varnames{i}).x(f1:f2));
    joint(i).y=mean(robot.(varnames{i}).y(f1:f2));
    joint(i).z=mean(robot.(varnames{i}).z(f1:f2));
    
    joint(i).qx=mean(robot.(varnames{i}).qx(f1:f2));
    joint(i).qy=mean(robot.(varnames{i}).qy(f1:f2));
    joint(i).qz=mean(robot.(varnames{i}).qz(f1:f2));
    joint(i).qw=mean(robot.(varnames{i}).qw(f1:f2));
    
    joint(i).tracked=mean(robot.(varnames{i}).tracked);
    
    robot.(varnames{i})=joint(i);    
end

robot.time=0;
robot.frame=1;
robot.framecount=1;

robot.(varnames{i}).q=[];
robot.(varnames{i}).angle=[];

RobotJointAngle

end


