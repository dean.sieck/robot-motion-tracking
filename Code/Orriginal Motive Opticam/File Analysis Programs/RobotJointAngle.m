function RobotJointAngle
%input quaternion matrix for each of 7 joints
%or input frame number for robot struct
global robot varnames
global solved

%if this is the first time running the program

TrackedJoints()

for frame=1:robot.framecount
    clear q
   
    %check which joints are solved 
    for i=1:7
        solved(i)=robot.(varnames{i}).solved(frame);
    end
        

    %Run joint angle assignments
    if solved==1 %if everything is completely solved
        [R]=SolvedJointAngle(frame);
    else
        [R]=UnsolvedJointAngle(frame);
    end
    
    
    %local quaternian qx,qy,qz
    q(:,:)=.5*[sign(R(3,2,:)-R(2,3,:)).*sqrt(max(R(1,1,:)-R(2,2,:)-R(3,3,:)+1,0)),...
        sign(R(1,3,:)-R(3,1,:)).*sqrt(max(R(2,2,:)-R(3,3,:)-R(1,1,:)+1,0)),...
        sign(R(2,1,:)-R(1,2,:)).*sqrt(max(R(3,3,:)-R(1,1,:)-R(2,2,:)+1,0))];
    %quaternian qw
    q(4,1:7)=.5*min(sqrt(max(R(1,1,:)+R(2,2,:)+R(3,3,:)+1,0)),2);
    %joint angle
    angle=acos(q(4,:)).*2*180/pi;
    
    %patch all joints with imaginary angles
    if ~isreal(angle)
        %bang head against wall
    end
    
    
    %adjust angles
    %base
    if q(3,1)<0
        angle(1)=-angle(1);
    end
    %joint 1
    if q(3,2)<0
        angle(2)=-angle(2);
    end
    %joint 3
    if q(2,3)>0
        angle(3)=-angle(3);
    end
    %joint 4
    if q(2,4)>0
        angle(4)=-angle(4);
    end
    %joint 5
    if q(2,5)>0
        angle(5)=-angle(5);
    end
    %joint 6
    if q(3,6)>0
        angle(6)=-angle(6);
    end
    %end effector
    if q(2,7)>0
        angle(7)=-angle(7);
    end
    
    % Assign values to robot struct
    for i=1:7
        robot.(varnames{i}).q(:,frame)=q(:,i);
        robot.(varnames{i}).angle(frame)=angle(i);
    end
end

end

function [R]=SolvedJointAngle(frame)
global robot varnames

% collect data
for i=1:7
    qx(i)=robot.(varnames{i}).qx(frame);
    qy(i)=robot.(varnames{i}).qy(frame);
    qz(i)=robot.(varnames{i}).qz(frame);
    qw(i)=robot.(varnames{i}).qw(frame);
end

% Calculate Joint Angles

%find rotation matrix to origin
for i=1:length(qx)
    Ro(:,:,i)=[2*(qw(i)^2+qx(i)^2)-1,     2*(qx(i)*qy(i)-qw(i)*qz(i)),    2*(qx(i)*qz(i)+qw(i)*qy(i));...
        2*(qx(i)*qy(i)+qw(i)*qz(i)),  2*(qw(i)^2+qy(i)^2)-1,          2*(qy(i)*qz(i)-qw(i)*qx(i));...
        2*(qx(i)*qz(i)-qw(i)*qy(i)),  2*(qy(i)*qz(i)+qw(i)*qx(i)),    2*(qw(i)^2+qz(i)^2)-1];
end

%set each rotation matrix according to robot base as origin
for i=2:length(qx)
    Rb(:,:,i)=(Ro(:,:,1))\Ro(:,:,i); %matlab recomended using A\b instead of inv(A)*b
end

%set each rotation matrix according to previous joint
for i=length(qx):-1:3
    R(:,:,i)=(Rb(:,:,i-1))\Rb(:,:,i); %matlab recomended using A\b instead of inv(A)*b
end
R(:,:,2)=Rb(:,:,2);
R(:,:,1)=Ro(:,:,1);

robot.base.Ro(:,:,frame)=R(:,:,1);
for i=2:7
    %update robot matrices
    robot.(varnames{i}).Ro(:,:,frame)=Ro(:,:,i);
    robot.(varnames{i}).Rb(:,:,frame)=Rb(:,:,i);
    robot.(varnames{i}).R(:,:,frame)=R(:,:,i);
end

end

function [R]=UnsolvedJointAngle(frame)
global robot varnames solved
j=0;

if ~solved(1)
    UnsolvedBase(frame);
    solved(1)=1;
end

%Used solveAngle to solve for all rotation matrices that havent yet been solved
for i=find(~solved)
    solveAngle(i,frame);
end
%all quaternions are now found, so SolvedJointAngle can be used
[R]=SolvedJointAngle(frame);

%solve locations of all unsolved joints (note that for solved angles solved=0.5)
for u=find(solved~=1);
    switch u
        case {2,5,6} %joints can be solved both directions
            if (robot.(varnames{u+1}).solved(frame) && robot.(varnames{u-1}).solved(frame))%next joint is solved
                solvePosition(u,0,frame)
            elseif robot.(varnames{u+1}).solved(frame)
                solvePosition(u,1,frame)
            elseif robot.(varnames{u-1}).solved(frame)
                solvePosition(u,-1,frame)
            else
                disp('cant solve 3 consecutive missing joints yet');
            end
            
        case {3,4} %joints 3 & 4 can be solved from backwards direction only
            if robot.(varnames{u+1}).solved(frame)
                solvePosition(u,1,frame)
            else
                disp('cant solve j3 or j4 without next joint yet');
            end
        case (7) %end effector can be solved from forward direction only
            if robot.(varnames{u-1}).solved(frame)
                solvePosition(u,-1,frame)
            else
                disp('cant solve missing end effector & j6 yet');
            end
    end
end
%set each rotation matrix according to previous joint

end

function [Rb]=UnsolvedBase(frame)
global robot

for fl=frame:-1:1 %find last solved frame
    if robot.base.solved(fl) || fl==1;
        qyl=robot.base.qy(fl); %base rotates about y
        qwl=robot.base.qw(fl);
        xl=robot.base.x(fl);
        yl=robot.base.y(fl);
        zl=robot.base.z(fl);
        break
    end
end
for fn=frame:robot.framecount %find next solved frame
    if robot.base.solved(fn) || fn==robot.framecount;
        qyn=robot.base.qy(fn); %base rotates about y
        qwn=robot.base.qw(fn);
        xn=robot.base.x(fn);
        yn=robot.base.y(fn);
        zn=robot.base.z(fn);
        break
    end
end

%find angle change over time
tl=robot.time(fl);
tn=robot.time(fn);
dt=tn-tl;
dqw=(qwn-qwl)/dt;
dqy=(qyn-qyl)/dt;
dx=(xn-xl)/dt;
dy=(yn-yl)/dt;
dz=(zn-zl)/dt;

%find missing quaternions
for f=min(fl+1,frame):max(fn-1,frame)
    %assume constant rotational velocity for missing base
    qw=qwl+dqw*(robot.time(f)-tl);
    qy=qyl+dqy*(robot.time(f)-tl);
    %update qx & qz to create functional quaternions
    qz=sqrt(max((1-qw^2-qy^2)/2),0);
    qx=-qz;
    %update robot struct
    robot.base.qx(f)=qx;
    robot.base.qy(f)=qy;
    robot.base.qz(f)=qz;
    robot.base.qw(f)=qw;
    R=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
        2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,          2*(qy*qz-qw*qx);...
        2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
    robot.base.Ro(:,:,f)=R;
    %if joint 1 is tracked assign new locations to base
    if robot.j1.tracked(f)
        qx=robot.j1.qx(f);
        qy=robot.j1.qy(f);
        qz=robot.j1.qz(f);
        qw=robot.j1.qw(f);
        robot.j1.Ro(:,:,f)=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
            2*(qx*qy+qw*qz),     2*(qw^2+qy^2)-1,    2*(qy*qz-qw*qx);...
            2*(qx*qz-qw*qy),     2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
        solvePosition(1,1,frame)
    elseif robot.j3.tracked(f)
        %solve for j1 based on j3
        qx=robot.j3.qx(f);
        qy=robot.j3.qy(f);
        qz=robot.j3.qz(f);
        qw=robot.j3.qw(f);
        robot.j3.Ro(:,:,f)=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
            2*(qx*qy+qw*qz),     2*(qw^2+qy^2)-1,    2*(qy*qz-qw*qx);...
            2*(qx*qz-qw*qy),     2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
        solvePosition(2,1,frame)
        %assume that the base is directly under j1
        robot.base.x(f)=robot.j1.x(f);
        robot.base.y(f)=robot.j1.y(f)-.1625;
        robot.base.z(f)=robot.j1.z(f);
    else
        %assume linear movement
        robot.base.x(f)=xl+dx*(robot.time(f)-tl);
        robot.base.y(f)=yl+dy*(robot.time(f)-tl);
        robot.base.z(f)=zl+dz*(robot.time(f)-tl);
    end
    robot.base.solved(f)=1;
end
end

function solvePosition(u,s,frame)
global robot varnames

%solve for location

switch s
    case 0 %both adjecent joints are tracked
        %sovle backwards direction
        x= robot.(varnames{u+1}).x(frame);
        y= robot.(varnames{u+1}).y(frame);
        z= robot.(varnames{u+1}).z(frame);
        
        vo=[x;y;z];
        Ro=robot.(varnames{u+1}).Ro(:,:,frame);
        A=[Ro,vo;[0,0,0,1]];
        p1=robot.(varnames{u+1}).p1;
        p01=A*p1;
        
        %solve forwards direction
        x= robot.(varnames{u-1}).x(frame);
        y= robot.(varnames{u-1}).y(frame);
        z= robot.(varnames{u-1}).z(frame);
        
        vo=[x;y;z];
        Ro=robot.(varnames{u-1}).Ro(:,:,frame);
        A=[Ro,vo;[0,0,0,1]];
        p1=robot.(varnames{u-1}).p2;
        p02=A*p1;
        
        %average backwards and forwards
        p0=(p01+p02)./2;
    case 1 %next joint is tracked
        %sovle backwards direction
        x= robot.(varnames{u+1}).x(frame);
        y= robot.(varnames{u+1}).y(frame);
        z= robot.(varnames{u+1}).z(frame);
        
        vo=[x;y;z];
        Ro=robot.(varnames{u+1}).Ro(:,:,frame);
        A=[Ro,vo;[0,0,0,1]];
        p1=robot.(varnames{u+1}).p1;
        p0=A*p1;
    case -1 %previous joint is tracked
        %solve forwards direction
        x= robot.(varnames{u-1}).x(frame);
        y= robot.(varnames{u-1}).y(frame);
        z= robot.(varnames{u-1}).z(frame);
        
        vo=[x;y;z];
        if u~=2
            Ro=robot.(varnames{u-1}).Ro(:,:,frame);
        else
            Ro=robot.(varnames{u-1}).R(:,:,frame);
        end
        A=[Ro,vo;[0,0,0,1]];
        p1=robot.(varnames{u-1}).p2;
        p0=A*p1;
end

robot.(varnames{u}).x(frame)=p0(1);
robot.(varnames{u}).y(frame)=p0(2);
robot.(varnames{u}).z(frame)=p0(3);


robot.(varnames{u}).solved(frame)=robot.(varnames{u}).solved(frame)+.5;
end

function solveAngle(i,frame)
global robot varnames

%solve for relative rotation matrix assumming linear change
%collect last known data and next data
for fl=frame:-1:1 %find last solved frame
    if robot.(varnames{i}).solved(fl) 
        qzl=robot.(varnames{i}).q(3,fl); 
        qyl=robot.(varnames{i}).q(2,fl); 
        qwl=robot.(varnames{i}).q(4,fl);
        tl=robot.time(fl);
        break
    elseif fl==1;
        %no data yet, it doesnt matter much since it is the first frame
        qzl=0;
        qyl=0; 
        qwl=1;
        tl=robot.time(fl);
        break
    end
        
end
for fn=frame:robot.framecount %find next solveable frame
    if (robot.(varnames{i}).solved(fn) && robot.(varnames{i-1}).solved(fn)) || fn==robot.framecount
        SolvedJointAngle(fn);
        R=robot.(varnames{i}).R(:,:,fn);
        qyn=.5*sign(R(1,3)-R(3,1))*sqrt(max(R(2,2)-R(3,3)-R(1,1)+1,0));
        qzn=.5*sign(R(2,1)-R(1,2)).*sqrt(max(R(3,3)-R(1,1)-R(2,2)+1,0));
        qwn=.5*min(sqrt(max((R(1,1)+R(2,2)+R(3,3)+1),0)),2);
        tn=robot.time(fn);
        break
    end
        
end

%find angle change over time
dt=tn-tl;
dqw=(qwn-qwl)/dt;
dqz=(qzn-qzl)/dt;
dqy=(qyn-qyl)/dt;

%find missing quaternions
switch i
    case {2,6}     
        %find missing quaternions
        for f=min(fl+1,frame):max(fn-1,frame)
            %assume constant rotational velocity for missing (varnames{i})
            qw=qwl+dqw*(robot.time(f)-tl);
            qz=qzl+dqz*(robot.time(f)-tl);
            %update qx & qz to create functional quaternions
            qx=sqrt(max((1-qw^2-qz^2)/2,0));
            qy=-qx;
            %solve for rotation matrix relative to last joint
            robot.(varnames{i}).q(1,f)=qx;
            robot.(varnames{i}).q(2,f)=qy;
            robot.(varnames{i}).q(3,f)=qz;
            robot.(varnames{i}).q(4,f)=qw;
            R=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
                2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,     2*(qy*qz-qw*qx);...
                2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
            robot.(varnames{i}).R(:,:,f)=R;
        end
        
    case {3,4,5,7}
        for f=min(fl+1,frame):max(fn-1,frame)
            %assume constant rotational velocity for missing (varnames{i})
            qw=qwl+dqw*(robot.time(f)-tl);
            qy=qyl+dqy*(robot.time(f)-tl);
            %update qx & qz to create functional quaternions
            qx=sqrt(max((1-qw^2-qy^2)/2,0));
            qz=-qx;
            %solve for rotation matrix relative to last joint
            robot.(varnames{i}).q(1,f)=qx;
            robot.(varnames{i}).q(2,f)=qy;
            robot.(varnames{i}).q(3,f)=qz;
            robot.(varnames{i}).q(4,f)=qw;
            R=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
                2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,     2*(qy*qz-qw*qx);...
                2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
            robot.(varnames{i}).R(:,:,f)=R;
                      
        end
end

%Update Robot Struct
for f=min(fl+1,frame):max(fn-1,frame)
    
   
              
    %solve for Rotation matrix relative to the base
    if i==2
        robot.j1.Rb(:,:,f)=robot.j1.R(:,:,f);
        robot.j1.Ro(:,:,f)=robot.base.Ro(:,:,f)*robot.j1.Rb(:,:,f);
    else
        %solve for Rotation matrix relative to the base
        robot.(varnames{i}).Rb(:,:,f)=robot.(varnames{i-1}).Rb(:,:,f)*robot.(varnames{i}).R(:,:,f);
        %solve for Rotation matrix relative to the origin
        robot.(varnames{i}).Ro(:,:,f)=robot.base.Ro(:,:,f)*robot.(varnames{i}).Rb(:,:,f);
    end
    
    %solve for global quaternion values
    Ro=robot.(varnames{i}).Ro(:,:,f);
    Q(:)=.5*[sign(Ro(3,2)-Ro(2,3)).*sqrt(max(Ro(1,1)-Ro(2,2)-Ro(3,3)+1,0)),...
        sign(Ro(1,3)-Ro(3,1)).*sqrt(max(Ro(2,2)-Ro(3,3)-Ro(1,1)+1,0)),...
        sign(Ro(2,1)-Ro(1,2)).*sqrt(max(Ro(3,3)-Ro(1,1)-Ro(2,2)+1,0))];
    Qw=.5*min(sqrt(max(Ro(1,1)+Ro(2,2)+Ro(3,3)+1,0)),2);
            
    robot.(varnames{i}).qx(f)=Q(1);
    robot.(varnames{i}).qy(f)=Q(2);
    robot.(varnames{i}).qz(f)=Q(3);
    robot.(varnames{i}).qw(f)=Qw;
       
    %joint rotation is solved
    robot.(varnames{i}).solved(f)=0.5;
end

end

function TrackedJoints()
global robot varnames
    % figure out if joints were was actually tracked
   
for frame=1:robot.framecount
    
    err=.005;
    for i=2:6
            %ensure no imaginary matrices
            qx=robot.(varnames{i}).qx(frame);
            qy=robot.(varnames{i}).qy(frame);
            qz=robot.(varnames{i}).qz(frame);
            qw=sqrt(1-qx^2-qy^2-qz^2)*sign(robot.(varnames{i}).qw(frame));
            dqw=abs(qw-robot.(varnames{i}).qw(frame));                
            if (dqw>err) || ~isreal(qw)
                robot.(varnames{i}).tracked(frame)=0;
            end
            
        if [robot.(varnames{i}).tracked(frame),robot.(varnames{i-1}).tracked(frame),robot.(varnames{i+1}).tracked(frame)]
            %find difference between joints
            dx=robot.(varnames{i}).x(frame)-robot.(varnames{i-1}).x(frame);
            dy=robot.(varnames{i}).y(frame)-robot.(varnames{i-1}).y(frame);
            dz=robot.(varnames{i}).z(frame)-robot.(varnames{i-1}).z(frame);
            d1=sqrt(dx^2+dy^2+dz^2);
            d1=abs(d1-robot.(varnames{i}).d1);
            
            dx=robot.(varnames{i}).x(frame)-robot.(varnames{i+1}).x(frame);
            dy=robot.(varnames{i}).y(frame)-robot.(varnames{i+1}).y(frame);
            dz=robot.(varnames{i}).z(frame)-robot.(varnames{i+1}).z(frame);
            d2=sqrt(dx^2+dy^2+dz^2);
            d2=abs(d2-robot.(varnames{i}).d2);
                        
            if (d1>err && d2>err)
                robot.(varnames{i}).tracked(frame)=0;
            end
        end
    end
    dx=robot.j6.x(frame)-robot.end.x(frame);
    dy=robot.j6.y(frame)-robot.end.y(frame);
    dz=robot.j6.z(frame)-robot.end.z(frame);
    d2=sqrt(dx^2+dy^2+dz^2);
    if robot.j6.tracked(frame) && robot.end.tracked(frame) && abs(d2-robot.j6.d2)>err
        robot.end.tracked(frame)=0;
    end
end

%create new variable to check if a joint has been solved for
for i=1:7
    robot.(varnames{i}).solved=double(robot.(varnames{i}).tracked);
end

end
