%one program to acess all motion tracking programs cvreated for the robot

clc, clear all, close all

global names varnames robot closeEnd
addpath(genpath(pwd))

%variables that could be changed
closeEnd=false;

robot=struct();
names=["Base","Joint 1","Joint 3","Joint 4","Joint 5","Joint 6","End Effector"];
varnames={'base','j1','j3','j4','j5','j6','end'};

% Start Streaming or record data
record=0;

options=[...
    "0 - Calibrate Joint Locations"...
    "1 - Record a Streaming"...
    "2 - Compare Scans"...
    ];

disp(options')
selection=input('Enter Selection: ');
if isempty(selection)
    selection=1;
end

switch selection
    case 0  %Joint Calibration
        clc
        RobotJointPositionCalibration
        return
    case 1  %Motive Recording
        clc
        SetContainers
        MotiveRecording
        SaveRobotStruct
        RobotSketch        
    case 2  %Compare scans
    disp('code not written yet')
        
    otherwise
        disp('Invalid selection')
end




function SaveRobotStruct()
% save file if recorded
global robot varnames
clc
record=input('enter a file name to save new robot data: ','s');
if contains(record,'.') %if name contains a .
    clc
    disp('not a valid name')
    record=input('enter a file name to save new robot data: ','s');
end
if ~isempty(record);
    while  exist(['saved files\',record,'.mat'])
        fprintf('%s alreay exists\n',record)
        r=input('press enter to overwrite data, or enter a new name:','s');
        if contains(r,'.') %if name contains a .
            clc
            disp('not a valid name')
            r=input('enter a file name to save new robot data: ','s');
        end
        if isempty(r)
            break
        end
        record=r;
    end

        
        disp('File Saved')
        save(['saved files\',record],'robot','varnames');
end
end
