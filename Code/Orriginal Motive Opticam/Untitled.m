global robot varnames
format compact
clear r11 r12 r13 r21 r22 r23 r31 r32 r33 robot2

%creates new robot struct with new rotation matrices for each joint
%assumming every tenth frame is tracked

robot2=robot;
for i=2:7
    robot2.(varnames{i}).solved=zeros(1,robot2.framecount);
    for frame=1:10:robot2.framecount
        robot2.(varnames{i}).solved(frame)=robot.(varnames{i}).solved(frame);
    end
    robot.(varnames{i}).tracked=robot.(varnames{i}).solved;
end

for i=2:7
    for frame=robot.frame 
        if robot.(varnames{i}).solved(frame)
        switch i
            case {2,6}
                for fl=frame:-1:1 %find last solved frame
                    if robot2.(varnames{i}).solved(fl) || fl==1;
                        qzl=robot2.(varnames{i}).q(3,fl); %(varnames{i}) rotates about y
                        qwl=robot2.(varnames{i}).q(4,fl);
                        break
                    end
                end
                for fn=frame:robot2.framecount %find next solved frame
                    if robot2.(varnames{i}).solved(fn) || fn==robot2.framecount;
                        qzn=robot2.(varnames{i}).q(3,fn); %(varnames{i}) rotates about y
                        qwn=robot2.(varnames{i}).q(4,fn);
                        break
                    end
                end
                
                %find angle change over time
                tl=robot2.time(fl);
                tn=robot2.time(fn);
                dt=tn-tl;
                dqw=(qwn-qwl)/dt;
                dqz=(qzn-qzl)/dt;
                
                %find missing quaternions
                for f=min(fl+1,frame):max(fn-1,frame)
                    
                    if f==501
                    end
                                        
                    %assume constant rotational velocity for missing (varnames{i})
                    qw=qwl+dqw*(robot2.time(f)-tl);
                    qz=qzl+dqz*(robot2.time(f)-tl);
                    %update qx & qz to create functional quaternions
                    qy=sqrt((1-qw^2-qz^2)/2);
                    qx=-qz;
                    %update robot struct
                    robot2.(varnames{i}).q(1,f)=qx;
                    robot2.(varnames{i}).q(2,f)=qy;
                    robot2.(varnames{i}).q(3,f)=qz;
                    robot2.(varnames{i}).q(4,f)=qw;
                    R=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
                        2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,          2*(qy*qz-qw*qx);...
                        2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
                    robot2.(varnames{i}).R(:,:,f)=R;
                    robot2.(varnames{i}).solved(f)=.5;
                end
                
                
            case {3,4,5,7}
                for fl=frame:-1:1 %find last solved frame
                    if robot2.(varnames{i}).solved(fl) || fl==1;
                        qyl=robot2.(varnames{i}).q(2,fl); %(varnames{i}) rotates about y
                        qwl=robot2.(varnames{i}).q(4,fl);
                        break
                    end
                end
                for fn=frame:robot2.framecount %find next solved frame
                    if robot2.(varnames{i}).solved(fn) || fn==robot2.framecount;
                        qyn=robot2.(varnames{i}).q(2,fn); %(varnames{i}) rotates about y
                        qwn=robot2.(varnames{i}).q(4,fn);
                        break
                    end
                end
                
                %find angle change over time
                tl=robot2.time(fl);
                tn=robot2.time(fn);
                dt=tn-tl;
                dqw=(qwn-qwl)/dt;
                dqy=(qyn-qyl)/dt;
                
                %find missing quaternions
                for f=min(fl+1,frame):max(fn-1,frame)
                    %assume constant rotational velocity for missing (varnames{i})
                    qw=qwl+dqw*(robot2.time(f)-tl);
                    qy=qyl+dqy*(robot2.time(f)-tl);
                    %update qx & qz to create functional quaternions
                    qz=sqrt((1-qw^2-qy^2)/2);
                    qx=-qz;
                    %update robot struct
                    robot2.(varnames{i}).q(1,f)=qx;
                    robot2.(varnames{i}).q(2,f)=qy;
                    robot2.(varnames{i}).q(3,f)=qz;
                    robot2.(varnames{i}).q(4,f)=qw;
                    R=[2*(qw^2+qx^2)-1,     2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy);...
                        2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,     2*(qy*qz-qw*qx);...
                        2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1];
                    robot2.(varnames{i}).R(:,:,f)=R;
                    robot2.(varnames{i}).solved(f)=.5;
                end
                
        end
        end
    
    
    robot2.(varnames{i}).angle(frame)=acos(robot2.(varnames{i}).q(4,frame))*2*180/pi;
    %adjust angles
    switch i
        case 2
            %joint 1
            if qz<0
                robot2.(varnames{i}).angle(frame)=-robot2.(varnames{i}).angle(frame);
            end
        case {3,4,5,7}
            %joint 3
            if qy>0
                robot2.(varnames{i}).angle(frame)=-robot2.(varnames{i}).angle(frame);
            end
        case 6
            %joint 6
            if qz>0
                robot2.(varnames{i}).angle(frame)=-robot2.(varnames{i}).angle(frame);
            end
    end
    end
    
    
    
    
    figure (i-1)
    hold off   
   % plot(robot2.frame,robot2.(varnames{i}).q,'.')
    plot(robot2.frame,robot2.(varnames{i}).angle/180,'.') 
    hold on
   % plot(robot.frame,robot.(varnames{i}).q)
    plot(robot.frame,robot.(varnames{i}).angle/180)
    title(robot.(varnames{i}).name)
    
end


