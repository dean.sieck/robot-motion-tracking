function Save2Table
%Saves Robot struct to table 
%   stores frame and time
%   stores tracked, x,y,x, qx,qy,qz and angle for each joint 
%   stores x,y,z for containers

global varnames robot names

file=input('Enter Robot Table name: ','s');
while ~exist(['saved files\Robot Tables\',file,'.txt'])
    fprintf ('%s.txt does not exist',file)
    file=input('Enter Robot Table name: ','s');
    if isempty(file)
        return
    end
end

t=readtable(['saved files\Robot Tables\',file,'.txt']);
robot.frame=t.frame';
robot.time=t.time';
robot.framecount=length(robot.frame);

for i=1:7 %collect data for robot joints
    n=varnames{i};
    
    robot.(n).name=names{i};
    
    eval(sprintf("robot.%s.tracked=t.%s_tracked';",n,n))
    eval(sprintf("robot.%s.solved=t.%s_tracked';",n,n))
    
    eval(sprintf("robot.%s.x=t.%s_x';",n,n))
    eval(sprintf("robot.%s.y=t.%s_y';",n,n))
    eval(sprintf("robot.%s.z=t.%s_z';",n,n))
    
    eval(sprintf("robot.%s.qx=t.%s_qx';",n,n))
    eval(sprintf("robot.%s.qy=t.%s_qy';",n,n))
    eval(sprintf("robot.%s.qz=t.%s_qz';",n,n))
    eval(sprintf("robot.%s.qw=t.%s_qw';",n,n))
    
    
end

%determine number of containers
c=length(t.Properties.VariableNames);
c=(c-2-7*9)/3;

robot.rbnum=7+c;
robot.containers=c;

for i=8:robot.rbnum 
    n=varnames{i};
    
    robot.(n).name=names{i};
    
    %collect location data for containers 
    eval(sprintf("robot.%s.x=t.%s_x';",n,n))
    eval(sprintf("robot.%s.y=t.%s_y';",n,n))
    eval(sprintf("robot.%s.z=t.%s_z';",n,n))
end

for i=1:robot.framecount
    JointAngle(i);
end

end

