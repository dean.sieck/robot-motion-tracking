%IK of UR5e
%By Steven Lukow
% TODO:     Generalize
%%

%Housekeeping
clc
clear all
close all

%Setup test locations


%TestPointsY = [ones(1,numPoints+1)*testSize, linspace(testSize, -testSize, numPoints), ones(1,numPoints+1)*-testSize, linspace(-testSize, testSize, numPoints) ];
inc = 0;

%TestPoints = [linspace(-boxSize, boxSize, numPoints), ones(1,numPoints+1)*boxSize, linspace(boxSize, -boxSize, numPoints), ones(1,numPoints+1)*-boxSize];
shapeLoc = [-.8, .5,0.2];
cameraDistance = 0.3;


startingAngle = atan2d(shapeLoc(2),shapeLoc(1)) + 180;
shapeRadius = 0.0825;

circle = linspace(startingAngle,startingAngle+360,360);
xPossible = shapeLoc(1)+(shapeRadius+cameraDistance)*cosd(circle);
yPossible = shapeLoc(2)+(shapeRadius+cameraDistance)*sind(circle);
circleIndex = [];
zPoints = max(0.16, shapeLoc(3)-0.03):.1:shapeLoc(3)+0.3;

circleIndex = [];
for i = 1:length(circle)
    if sqrt(xPossible(i)^2 + yPossible(i)^2 + (zPoints(1)-0.15)^2) < .8 && sqrt(xPossible(i)^2 + yPossible(i)^2 + (zPoints(end))^2) < .8
        circleIndex = [circleIndex, i];
    end
end
viewPoints = startingAngle;
if length(circleIndex) >= 1
    for i=1:20:length(circleIndex)
        viewPoints = [viewPoints, circle(circleIndex(i))]; % startingAngle;
    end
else
    viewPoints = 0;
end

figure(1)
hold on
for z = zPoints
    plot3(cosd(viewPoints)*0.1 + shapeLoc(1), sind(viewPoints)*0.1 + shapeLoc(2), zeros(size(viewPoints)) + z, 'b*')

for desiredYaw = sort(viewPoints)%linspace(-pi/2, -pi/2 + 2*pi, numPoints+1)%[linspace(-boxSize, boxSize, numPoints), ones(1,numPoints+1)*boxSize, linspace(boxSize, -boxSize, numPoints), ones(1,numPoints+1)*-boxSize]
    inc = inc + 1;
    %Define default configuration
    wristUp = 0;

    %Define shape location and other parameters
    
    
    %Define distance from camera end effector to cylinder wall
    cameraD = 0.3;
    
    %Link Dimensions
    wristD = 0.09960141; %wristD = 0.0823;
    toolD = 0.170;
    L1 = .42499948; %L1 = 0.4257;
    L2 = 0.39214349;  %L2 = 0.39155;
    
    %Combination lengths of tool and camera distance
    comboD = wristD + toolD + cameraD;
    camTool = toolD + cameraD;
    baseSideD = 0.13779926- 0.0059539; %baseSideD = 0.0997;
    
    %Calculate first link location and rotation
    %desiredYaw = startingAngle;
    desiredPitch = 0;
    %z = sind(desiredPitch)*comboD;
    rotateD = cosd(desiredPitch)*comboD;
    rotateD2 = cosd(desiredPitch)*camTool;
    cameraWrist = [shapeLoc(1) + rotateD*cosd(desiredYaw), shapeLoc(2) + rotateD*sind(desiredYaw),z];
    
    %IK plane target point
    desiredPoint1 = [shapeLoc(1) + rotateD*cosd(desiredYaw), shapeLoc(2) + rotateD*sind(desiredYaw), z + 0.101];
    desiredPoint2 = [shapeLoc(1) + rotateD*cosd(desiredYaw), shapeLoc(2) + rotateD*sind(desiredYaw),  z - 0.101];
    testAgain = 1;
    
    wristUp(inc) = 1;
    desiredPoint = desiredPoint1;
    
    %Find angles until a joint configuration is found
    while (testAgain == 1)
        %Find useful lengths for RRR IK
        xyD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2);
        xyzD = sqrt(desiredPoint(1)^2 + desiredPoint(2)^2 + desiredPoint(3)^2);
        linkD = sqrt(xyD^2 -(baseSideD)^2);
        if isreal(linkD)
            %Calculate J2,J3
            %theta_1 = asind(linkD/xyD);
            %theta_2 = atand(desiredPoint(2)/desiredPoint(1));
            
            %Work out RRR plane orientation
            x = desiredPoint(1);
            y = desiredPoint(2);
            d = baseSideD;
            
            %Find points tangent to base circluar motion
            a1 = (2*(d^2)*y + sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
            a2 = (2*(d^2)*y - sqrt((2*(d^2)*y)^2 - 4*((x^2)+(y^2))*((-d^2)*((x^2)-(d^2)))))/(2*((x^2)+(y^2)));
            b1 = sqrt((d^2)-(a1^2));
            b2 = sqrt((d^2)-(a2^2));
            if desiredPoint(1) >= 0 && desiredPoint(2) >= -baseSideD
                bTheta = atan2d(a2,b2);
            elseif desiredPoint(1) >= 0 && desiredPoint(2) <= baseSideD
                bTheta = -(atan2d(a2,b2)+180);
            elseif desiredPoint(1) <= 0 && desiredPoint(2) <= baseSideD
                bTheta = 180-((atan2d(a1,b1)));
            else
                bTheta = (atan2d(a1,b1));
            end
         
            %Find starting and ending point of RRR
            sP = [baseSideD*cosd(bTheta),baseSideD*sind(bTheta),0.1625];
            dZ = desiredPoint(3)-sP(3);
            
            %Two main joints
            theta2 = -acosd((linkD^2 + dZ^2 - L1^2 - L2^2)/(2*L1*L2));
            if ~isreal(theta2) || ~isreal((L2*sind(theta2))) || ~isreal((L1 + L2*cosd(theta2)))
                fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                return;
            end
            theta1 =  (atan2d(dZ,linkD)-atan2d((L2*sind(theta2)),(L1 + L2*cosd(theta2))));
            if ~isreal(theta1)
                fprintf('Error: Target is out of range (non-real joint angle detected)\n');
                return;
            end
            if wristUp(inc) == 0
                break
            end
            if ((theta1 + theta2) > 20) && (wristUp(inc) == 1)
                testAgain = 1;
                wristUp(inc) = 0;
                desiredPoint = desiredPoint2;
                fprintf('Should flip?\n')
            else
                wristUp(inc) = 1;
                testAgain = 0;
                
            end
        else
            if wristUp(inc) == 1
                testAgain = 1;
                wristUp(inc) = 0;
                desiredPoint = desiredPoint2;
            else
                fprintf('Object too close!\n');
                return
            end
        end
        
    end
    
    %Plot arm simulation

    
    pbaspect([1 1 1])
    %plot3([0,desiredPoint(1)],[0,desiredPoint(2)], [0,desiredPoint(3)], '*', 'color', 'c')
    plot3([0,0],[0,0], [0,sP(3)], 'linewidth', 3, 'color', 'b')
    plot3([0,sP(1)],[0,sP(2)], [sP(3),sP(3)], 'b', 'linewidth', 2.5)
    
    plot3([sP(1), sP(1)+L1*cosd(theta1)*cosd(bTheta+90)],[sP(2), sP(2)+L1*cosd(theta1)*sind(bTheta+90)],[sP(3), L1*sind(theta1) + sP(3)], 'b', 'linewidth', 2)
    elbow = [L1*cosd(theta1)*cosd(bTheta+90), L1*cosd(theta1)*sind(bTheta+90), L1*sind(theta1) + sP(3)];
    wrist = [L2*cosd(theta2+theta1)*cosd(bTheta+90), L2*cosd(theta2+theta1)*sind(bTheta+90), L2*sind(theta2+theta1)];
    plot3([sP(1)+L1*cosd(theta1)*cosd(bTheta+90), elbow(1)],[sP(2)+L1*cosd(theta1)*sind(bTheta+90), elbow(2)],[L1*sind(theta1) + sP(3), elbow(3)], 'b', 'linewidth', 1.5)
    
    plot3([elbow(1), elbow(1) + wrist(1)],[elbow(2), elbow(2) + wrist(2)],[elbow(3), elbow(3) + wrist(3)],'b')
    plot3([elbow(1) + wrist(1), elbow(1) + wrist(1) + baseSideD*cosd(bTheta)],[ elbow(2) + wrist(2), elbow(2) + wrist(2) + baseSideD*sind(bTheta)],[elbow(3) + wrist(3), elbow(3) + wrist(3)],'b', 'linewidth', 1.2)
    
    plot3([shapeLoc(1), cameraWrist(1)],[shapeLoc(2), cameraWrist(2)],[z, cameraWrist(3)], 'c:', 'linewidth', 1)
    if wristUp(inc) == 1
        plot3([cameraWrist(1), cameraWrist(1)],[cameraWrist(2), cameraWrist(2)],[cameraWrist(3), cameraWrist(3)+0.0997], 'c')
    else
        plot3([cameraWrist(1), cameraWrist(1)],[cameraWrist(2), cameraWrist(2)],[cameraWrist(3), cameraWrist(3)-0.0997], 'c')
    end
    
    
    [cX, cY, cZ] = cylinder();
    [sX, sY, sZ] = sphere();
    surf(cX*shapeRadius+shapeLoc(1), cY*shapeRadius+shapeLoc(2), cZ*.2032 + shapeLoc(3))
    %surf(cX*0.064, cY*0.064, cZ*0.086)
    %surf(cX, cY, cZ)
    %surf(sX*.05, sY*.05, sZ*.05 + 0.086)
    if inc == 1
        view(0, 90)
    end
    axis([-1,.2,-.2,1, -.3, .9])
    xlabel('X')
    ylabel('Y')
    pause(0.05)
    
    %Write joint angles for the robot to use
    J1(inc) = bTheta -90;%-360;
    J2(inc) = theta1-180;
    J3(inc) = theta2;
    J4(inc) = abs(J2(inc)) + abs(J3(inc))-360;
    J5(inc) =  J1(inc) - desiredYaw + 90+180;
    if J5(inc) >=90
        J5(inc) = J5(inc)-360;
    end
    if J5(inc) < -190
        J5(inc) = J5(inc) + 360;
    end
    
    J6(inc) = 45;
    if wristUp(inc) == 0
        J1(inc) = bTheta-90;
        J4(inc) =J4(inc) + 180;
        J5(inc) =(desiredYaw-180)-bTheta;
        if J5(inc) < -90
            J5(inc) = J5(inc) + 360;
        end
        J6(inc) =J6(inc) - 180;
    end
    
    %fprintf('%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n', J1, J2, J3, J4, J5, J6)
    
end
end
allJoints = [J1;J2;J3;J4;J5;J6];
%%
%Connect to robot and run motions

tempData = input('Press Enter to begin moving robot');
global robot
robot = robotSetup();


for j = 1:length(J1)
    moveJointsTimeJ(J1(j), J2(j), J3(j), J4(j), J5(j), J6(j), 4);
    %tempData = input('Press Enter to stop');
    pause(5);
    %stopTool();
end
