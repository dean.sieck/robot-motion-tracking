
function plotjoints( ~ , evnt )
	% The event callback function executs each time a frame of mocap data is delivered.
	% to Matlab. Matlab will lag if the data rate from the Host is too high.
	% A simple animated line graphs the x, y, z position of the first rigid body on the Host.

	
	% Note - This callback uses the gobal variables px, py, pz from the NatNetEventHandlerSample.
	global px
	global py
	global pz
	global jl JL 
    global ID names
	
	% local variables
	persistent frame1
	persistent lastframe1
	scope = 1.5;
    
    % base location
    joint=evnt.data.RigidBodies( ID(1) );
    x(1) = double(joint.x);
	z(1) = double( joint.y);
	y(1) = -double( joint.z);
    
    
for i=2:7
    %get frame number
	frame1 = double( evnt.data.iFrame );
	if ~isempty( frame1 ) && ~isempty( lastframe1 )
		if frame1 < lastframe1
			px(i-1).clearpoints;
			py(i-1).clearpoints;
			pz(i-1).clearpoints;
		end
	end
    if ID(i)==0
        clc
        fprintf('ERROR: %s not defined with ID = %d\n',names(i),i)
        return
    end
    
    joint=evnt.data.RigidBodies( ID(i) );
	% Get the rigid body position
	x(i) = double( joint.x);
	z(i) = double( joint.y);
	y(i) = -double( joint.z);
    frame = frame1;
    time=toc;
        
     %if its tracked
    if  evnt.data.RigidBodies(ID(i)).MeanError ~=0
        %Fill the animated line's queue with the rb position
        px(i-1).addpoints( time , x(i) );
        py(i-1).addpoints( time , y(i) );
        pz(i-1).addpoints( time , z(i) );
    end

	
	% set the figure and subplot to graph the data
	set(JL , 'CurrentAxes' , jl(i-1))

	% Dynamically move the axis of the graph
	axis( [ -10 + time ,  time , - scope , scope ] );

	
	% Draw the data to a figure
	drawnow
    
	

	% Update lastframe
	lastframe1 = frame1;
end
    x=round(x'*1000)/10; y=round(y'*1000)/10; z=round(z'*1000)/10; Joint=names(1:7)';
    clc
    table(Joint,x,y,z)
    
end  % eventcallback1
