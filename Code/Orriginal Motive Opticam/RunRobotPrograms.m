%one program to acess all motion tracking programs cvreated for the robot

clc, clear all, close all

global names varnames robot closeEnd
addpath(genpath(pwd))

%variables that could be changed
closeEnd=false;


robot=struct();
names=["Base","Joint 1","Joint 3","Joint 4","Joint 5","Joint 6","End Effector"];
varnames={'base','j1','j3','j4','j5','j6','end'};

%% collect container data
robot.containerLoc=SetContainers;

%% Start Streaming or record data
record=0;

options=[...
    "0 - Calibrate Joint Locations"...
    "1 - Record a Streaming"...
    "2 - Live Streaming"...
    "3 - Upload a Motive File (.csv)"...
    "4 - Upload a saved robot file (.mat)"...
    "5 - Upload robot file from Robot Table (.txt)"
    ];

disp(options')
selection=input('Enter Selection: ');

switch selection
    case 0  %Joint Calibration
        clc
        RobotJointPositionCalibration
        return
    case 1  %Motive Recording
        clc
        MotiveRecording
    case 2  %Motive Streaming
%         c=1;
%         while c==1
            clc, close all
            MotiveStreaming
%             clc
%             c=input('Enter 1 to perform anther streaming ');
%         end
        return        
    case 3  %Upload Motive File')
        clc
        MotiveFileToRobotStruct
    case 4 
        load (['saved files\',input('Enter File Name: ','s'),'.mat'])
    case 5
        msgbox('Upload robot file from table not written yet')
end

%% If a robot file was created run analysis files on it
if false   
selection=1;
selected=[];
while selection~=0;
    clc    
    options=[...
        "0 - Exit"...
        "1 - Save Data as a table file"...
        "2 - Sketch Robot Movement"...
        "3 - Average Robot Data"...   
        "4 - Run Untitled Program"...
        ];
    if ~isempty(selected)
        fprintf('Previous Selections:')
        disp(selected)
    end
    disp('Using Created Recording')
    disp(options')
    selection=input('Enter Selection: ');
    switch selection
        case 1 
            selected=[selected,1];
            Save2Table;
        case 2 
            selected=[selected,2];
            RobotSketch;
        case 3
            selected=[selected,3];
            AverageRobotData;
        case 4
            selected=[selected,4];
            Untitled
            return
    end
end
end

%% save file if recorded

clc
record=input('enter a file name to save new robot data: ','s');
if contains(record,'.') %if name contains a .
    clc
    disp('not a valid name')
    record=input('enter a file name to save new robot data: ','s');
end
if ~isempty(record);
    while  exist(['saved files\',record,'.mat'])
        fprintf('%s alreay exists\n',record)
        r=input('press enter to overwrite data, or enter a new name:','s');
        if contains(r,'.') %if name contains a .
            clc
            disp('not a valid name')
            r=input('enter a file name to save new robot data: ','s');
        end
        if isempty(r)
            break
        end
        record=r;
    end

        
        disp('File Saved')
        save(['saved files\',record],'robot','varnames');
end

