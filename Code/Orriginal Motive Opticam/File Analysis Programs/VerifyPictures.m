function verifyPictures()
global robot varnames

files=["Test1L","Test1R",...
    "Test2L","Test2L_Shake",...
    "Test2R","Test2R_Shake",...
    "Test3_Left","Test3_Left_Shake",...
    "Test3_Right","Test3_Right_2","Test3_Right_Shake",...
    "Test4_Left","Test4_Left_Shake",...
    "Test4_Right","Test4_Right_Shake",...
    "Test5_Left","Test5_Left_Shake",...
    "Test5_Right","Test5_Right_Shake"];
% 
% files=["Test1L"];
% for file=1:length(files)
%     change=0;
%     load(files(file))
    i=1;
    frame=1;
    length(robot.pic)
    while i<=length(robot.pic)
        lastframe=frame;
        frame=round(mean(robot.pic(i).frames));
        [isPic,d(i),D(i),da(i)]=isPicture(frame, lastframe);
        if ~isPic
            robot.pic(i)=[];
            change=1;
        else
            i=i+1;
        end
    end
    
%    fprintf('%d pictures in %s\n',length(robot.pic),files(file))
        fprintf('%d pictures\n',length(robot.pic))

%     input('')
    if length(robot.pic)>48 && false 
        i=1;
        frame=1;
        while i<=length(robot.pic)
            lastframe=frame;
            frame=round(mean(robot.pic(i).frames));
            sketchPic(frame)
            fprintf('\npicture: %d\n',i)
            cx=robot.end.x(frame); cy=robot.end.y(frame); cz=robot.end.z(frame);
            lx=robot.end.x(lastframe); ly=robot.end.y(lastframe); lz=robot.end.z(lastframe);
            da=(acos(robot.end.qw(frame))-acos(robot.end.qw(lastframe)))*2*180/pi;
            fprintf('current end location( %.2f, %.2f, %.2f )\n',cx,cy,cz)
            fprintf('current end location( %.2f, %.2f, %.2f )\n',lx,ly,lz)
            fprintf('difference %.3f cm and %.2f degrees \n',sqrt ((cx-lx)^2+(cy-ly)^2+(cz-lz)^2)*100,da)            
            delete=input('enter 1 if not a picture');
            if delete==1
                robot.pic(i)=[];
            else
                i=i+1;
            end
        end
    end

%     if change
% %         save(['saved files\',sprintf('%s',files(file))],'robot','varnames');
%     end
    
%end
end

function sketchPic(frame)
global robot varnames
%Draws a sketch of the robot at picture frame
range=1.5;

%setup plot
%figure ('Name','Robot Sketch','Position',[1921 177 1280 948])


%initialize line for sketch
sketch=plot3((1:7),(1:7),(1:7),'r');
hold on
p=plot3(0,0,0);
%initialize rigid body markers
p(1)=plot3(0,0,0,'Marker','s','Color','r','MarkerSize',10);%base
for i=2:6
    p(i)=plot3(0,0,0,'Marker','d','Color','r'); %Joints
end
p(7)=plot3(0,0,0,'Marker','p','Color','r','MarkerSize',10); %end Effector
for i=1:robot.containers
    p(i+7)=plot3(0,0,0,'Marker','o','Color','b','MarkerFaceColor','c');%containers
end
axis([-1,1,-1,1,-1,1]);
xlabel('x');ylabel('y');zlabel('z');
axis([robot.base.x(frame)-range,robot.base.x(frame)+range,robot.base.y(frame)-range,robot.base.y(frame)+range,robot.base.z(frame)-range,robot.base.z(frame)+range]);


for i=1:7 %for all joints
    %make arrays for line
    x(i)=robot.(varnames{i}).x(frame);
    y(i)=robot.(varnames{i}).y(frame);
    z(i)=robot.(varnames{i}).z(frame);
    
    %update rigid body markers
    p(i).XData=x(i);
    p(i).YData=y(i);
    p(i).ZData=z(i);
    if robot.(varnames{i}).tracked(frame)==true
        p(i).MarkerFaceColor='r';
    else
        p(i).MarkerFaceColor='w';
    end
end

if robot.end.pic(frame)
    p(7).Color='b';
else
    p(7).Color='r';
end

for i=1:robot.containers %for all containers
    p(i+7).XData=robot.containerLoc(i,1);
    p(i+7).YData=robot.containerLoc(i,2);
    p(i+7).ZData=robot.containerLoc(i,3);
end
%update line
sketch.XData=x;
sketch.YData=y;
sketch.ZData=z;

hold off
title(sprintf('Time: %.01f s',robot.time(frame)));
end


function [pic,d,D,da]=isPicture(frame, lastframe)
global robot
pic =1;
        %distance between container and end
        dx=robot.end.x(frame)-robot.containerLoc(:,1);
        dy=robot.end.y(frame)-robot.containerLoc(:,2);
        dz=robot.end.z(frame)-robot.containerLoc(:,3);
        d=min(sqrt(dx.^2+dy.^2+dz.^2));
        if d>.5            
            pic=0;
        end
        
        %distance between last picture (or first frame) and current picture
        Dx=robot.end.x(frame)-robot.end.x(lastframe); 
        Dy=robot.end.y(frame)-robot.end.y(lastframe); 
        Dz=robot.end.z(frame)-robot.end.z(lastframe); 
        D=sqrt(Dx^2+Dy^2+Dz^2)*100;
        da=abs((acos(robot.end.qw(frame))-acos(robot.end.qw(lastframe)))*2*180/pi);
        if D<10 || da<10
            pic=0;
        end
        
        
end
