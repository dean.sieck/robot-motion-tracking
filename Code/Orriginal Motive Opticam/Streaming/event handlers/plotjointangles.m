
 function oldplotjointangles( ~ , evnt )
	% The event callback function executs each time a frame of mocap data is delivered.
	% to Matlab. Matlab will lag if the data rate from the Host is too high.
	% A simple animated line graphs the x, y, z position of the first rigid body on the Host.

	
	% Note - This callback uses the gobal variables px, py, pz from the NatNetEventHandlerSample.
	global rx
	global ry
	global rz
	global ja JA 
    global ID names
	
	% local variables
	persistent frame1
	persistent lastframe1
	scope = 180;

for i=2:7
        %get frame number
	frame1 = double( evnt.data.iFrame );
	if ~isempty( frame1 ) && ~isempty( lastframe1 )
		if frame1 < lastframe1
			rx(i-1).clearpoints;
			ry(i-1).clearpoints;
			rz(i-1).clearpoints;
		end
    end


    if ID(i)==0
        clc
        fprintf('ERROR: %s not defined with ID = %d\n',names(i),i)
        return
    end
	% Get the rb rotation
	rb = evnt.data.RigidBodies( ID(i) );
	qx = rb.qx;
	qy = rb.qy;
	qz = rb.qz;
	qw = rb.qw;
	
	q = quaternion( qx, qy, qz, qw );
	qRot = quaternion( 0, 0, 0, 1);
	q = mtimes( q, qRot);
	a = EulerAngles( q , 'zyx' );
	x(i) = a( 1 ) * -180.0 / pi;
	y(i) = a( 2 ) * 180.0 / pi;
	z(i) = a( 3 ) * -180.0 / pi;
	frame = frame1;
    time=toc;   
    
     
    %if its tracked
    if  evnt.data.RigidBodies(ID(i)).MeanError ~=0
        % Fill the animated line's queue with the rb position
        rx(i-1).addpoints( time , x(i) );
        ry(i-1).addpoints( time , y(i) );
        rz(i-1).addpoints( time , z(i) );
    end

	
	% set the figure and subplot to graph the data
	set(JA , 'CurrentAxes' , ja(i-1))

	% Dynamically move the axis of the graph
	axis( [ time-10 ,  time , - scope , scope ] );

	
	% Draw the data to a figure
	drawnow
    
	

	% Update lastframe
	lastframe1 = frame1;
    
end
    
end  % eventcallback1
