function [ ] = robotZero()
%moveToLocation This function moves the arm to a location defined by the
%parameters x, y, z (in meters) and rx, ry, rz (in radians).
    global robot
    A = [0,-pi/2,0,-pi/2,0,0];
    fprintf(robot, 'stopl(0.3)\n');
    pause(0.5)
    fprintf(robot, 'movej([%.3f, %.3f, %.3f, %.4f, %.4f, %.4f], a=0.1,v=0.1, t=10.0)\n',A);
end

