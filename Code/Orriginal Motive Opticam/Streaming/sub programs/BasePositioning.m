function BasePositioning(natnetclient)

global x z angle base ang ID


loc =readtable('saved files\__SavedBaseLocations__.txt');

%selection options
fprintf('-1 - Delete Location\n 0 - New Location\n')
for i =2:height(loc)
    fprintf(' %d - %s\n',i-1,loc.LocationName{i})
end

select=input('\nEnter selection: ');
while select == -1 %Delete entry
        delete=input('Delete Row: ');
        loc(delete+1,:)=[];
        clc
        fprintf('-1 - Delete Location\n 0 - New Location\n')
        for i =1:height(loc)
            fprintf(' %d - %s\n',i,loc.LocationName{i})
        end
        writetable(loc,'saved files\__SavedBaseLocations__.txt')
        select=input('\nEnter selection: ');
end

clc
switch select
    case 0 %new entry   
        disp('enter coordinates as shown in Motive (mm)')
        disp('or press enter to use robot base current location')
        x=input('\nEnter X Location: ')/1000;
        if ~isempty(x)
            z=input('Enter Z Location: ')/1000;
            angle=input('Enter Angle: ');
        else
            clc
            disp('Robot Base Position Recorded')
            b=natnetclient.getFrame.RigidBody(ID(1));
            x=b.x;
            z=b.z;
            angle=acos(b.qw)*2*180/pi;
        end
        name=input('Location Name (if saving): ','s');
        if ~isempty(name)
            loc=[loc;{name,x,z,angle}];
            writetable(loc,'saved files\__SavedBaseLocations__.txt')
        end
    otherwise %selected entry
        x=loc.X(select+1);
        z=loc.Z(select+1);
        angle=loc.Angle(select+1);
        disp(loc(select+1,:))
end
%create a line for angle orientation
ax=sind(angle)/10+x;
az=cosd(angle)/10+z;
%create an image of locations
f=figure(1);
f.Position=[1921 177 1280 948];
plot(x,z,'Marker','o','LineWidth',2,'color','b')
hold on
plot([x,ax],[z,az],'color','b')
hold off
title( 'Base Location' );
xlabel( 'X Direction (m)' );
ylabel( 'Z Direction (m)' );
range=1;
axis([x-range,x+range,z-range,z+range])

base = animatedline();
base.MaximumNumPoints = 1;
base.Marker = '.';
base.MarkerSize = 20;
base.Color = 'r';

ang=animatedline();
ang.MaximumNumPoints = 2;
ang.Color = 'r';




       

end

