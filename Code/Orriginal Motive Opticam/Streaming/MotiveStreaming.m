
% Optitrack Matlab / NatNet Event Handler Sample
%  Requirements:
%   - OptiTrack Motive 2.0 or later
%   - OptiTrack NatNet 3.0 or later
%   - Matlab R2013

% This connects to the server and runs one of several programs showing
% robot location data

function MotiveStreaming

global names ID 

% addpath('event handlers')
% addpath('sub programs')
% addpath('../saved files')

%names=["Robot Base","Joint 1","Joint 3","Joint 4","Joint 5","Joint 6","End Effector"];
%respective joint ID numbers: [1,2,3,4,5,6,7];

% create an instance of the natnet client class
fprintf( 'Connecting to Natnet Client\n' )
natnetclient = natnet;
natnetclient.HostIP = '127.0.0.1';
natnetclient.ClientIP = '127.0.0.1';
natnetclient.ConnectionType = 'Multicast';

% connect the client to the server (multicast over local loopback) -
% modify for your network
while natnetclient.IsConnected == 0
    natnetclient.connect;
    if ( natnetclient.IsConnected == 0 )
        fprintf( 'Client failed to connect\n' )
        fprintf( '\tMake sure the host is connected to the network\n' )
        fprintf( '\tand that the host and client IP addresses are correct\n\n' )
    end
end

%update lists to match motive
rbnum=1;
while ~isempty(natnetclient.getFrame.RigidBody(rbnum))
   rbID=natnetclient.getFrame.RigidBody(rbnum).ID;
    % make joint locations match with the rigid body number
    ID(rbID)=rbnum;
    %add names for containers
    if rbID>length(names)
        names=[names,sprintf('Container %d',rbID-7)];   
    end
    rbnum=rbnum+1;
end

clc
options=[...
    "1 - Position Robot Base"...
    "2 - Measure Distances From End Effector to Containers"...     
    "3 - Plot Joint Locations"...
    "4 - Plot Quaternions"...
    "5 - Plot Joint Angles"...
    ];

disp(options')
%select an option
select=input ('enter selection: ');
clc
switch select
    case 1
        disp('Base Positioning')
        BasePositioning(natnetclient);
        natnetclient.addlistener( 1 , 'basepositioning');
    case 2
        disp('Measure Distances From End to Containers')
        PlotContainerLocations;
        natnetclient.addlistener( 1 , 'containerlocations');
    case 3
        disp('Plot Joint Locations')
        CreateJointPlots;
        natnetclient.addlistener( 1 , 'plotjoints' );
    case 4
        disp('Plot Quaternions')
        CreateQuaternionPlots;
        natnetclient.addlistener( 1 , 'plotquaternions' );        
    case 5
        disp('Plot Joint Angles')
        CreateJointAnglePlots;
        natnetclient.addlistener( 1 , 'plotjointangles' );
    otherwise
        return
      
end

tic
natnetclient.enable(0)
% run Program until stopped
input('Press Enter to Stop Program')
natnetclient.disable(0)

end


