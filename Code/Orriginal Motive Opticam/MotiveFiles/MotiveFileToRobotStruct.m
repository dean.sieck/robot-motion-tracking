function MotiveFileToRobotStruct
%MotiveFile takes the name of a .csv file produced by matlab and transforms
%it into two files for the frame data and the rigidbody data.

%Returns matrices of x,y,z location of robot head and containers with
%respect to robot base and the number of containers, framecount and the table with data from framecount 



% frame: Details frame of exported data in the following format:
%    -- Frame Index (integer)
%    -- Time Stamp (double) in seconds
%    -- Rigid Body Count (integer) number of rigid bodies tracked in current frame
%    -- Rigid Body ID, Position, Quaternion Orientation, and Euler Angle Orientation >for each tracked rigid body< (ID, x,y,z, qx, qy, qz, qw, yaw, pitch, roll)
%    -- Marker Count (integer) Count of all visible markers in frame
%    -- Marker Detail >for each reconstructed 3D marker< (x,y,z,id,name)

% rigidbody: Extended information for current frame:
%    -- Frame Index (integer)
%    -- Time Stamp (double) in seconds
%    -- Name (string)
%    -- ID   (integer)
%    -- Frames Since Last Tracked (integer). 0=currently tracked
%    -- Marker Count (integer)
%    -- Rigid Body Marker Location and ID >for each marker in rigid body< (x,y,z,id)
%    -- Point Cloud Marker corresponding to each rigid body marker (x,y,z). (NAN,NAN,NAN) used for missing Point Cloud Markers.
%    -- Marker Tracked (integer) for each marker. 1=track, 0=untracked.
%    -- Marker Quality (float) for each marker. 1.0=Highest Quality, 0.0=Lowest Quality
%    -- Mean Error (in mm/marker)


global robot
%% open the files
    filename=input('Enter .csv File Name: ','s');  
    framename=strcat('MotiveFiles\',filename,'_frame.txt');
    rigidbodyname=strcat('MotiveFiles\',filename,'_rigidbody.txt');

    if exist(framename, 'file')
      delete(framename)
      delete(rigidbodyname)
    end

    if exist([filename,'.csv'],'file')
        fileID=fopen([filename,'.csv'],'r');
        frameID=fopen(framename,'a');
        rigidbodyID=fopen(rigidbodyname,'a');
    else
        return
    end

    ln=0;


    %% put data in respective files
    for i = 1:41
        fgetl(fileID);
    end

    a=fgetl(fileID); 
    version=sscanf(a,'info,version,%f');
    a=fgetl(fileID); 
    framecount=sscanf(a,'info,framecount,%d');
    a=fgetl(fileID); 
    rigidbodycount=sscanf(a,'info,rigidbodycount,%d');
    fgetl(fileID);
    


    for i=1:rigidbodycount
        a=fgets(fileID);
    end

    while (a~=-1)
        ln=ln+1;
        if mod(ln,rigidbodycount+1)==1
            fprintf(frameID,'%s',a);
        else
            fprintf(rigidbodyID,'%s',a);
        end
        a=fgets(fileID);
    end

    fclose('all');

    
    %% Make tables
    % and Re-orient to make robot base the origin 
    data=readtable(framename);
    data.Properties.VariableNames(2:4)={'Frame','Time','RigidBodyCount'};
    rigidbodycount=data.RigidBodyCount(1);
    robot.containers=0;
    robot.time=data.Time;
    robot.frame=data.Frame;
    robot.framecount=framecount;
    robot.rbnum=rigidbodycount;
    
    j=5;
    for i=1:rigidbodycount
        rbID=data{1,j};
        switch rbID
            case (1)
                %first object is robot base
                data.Properties.VariableNames(j:j+7)={'RobotBase','Xb','Yb','Zb','qxb','qyb','qzb','qwb'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxb','ryb','rzb'}; j=j+3;
                robot.base.name="Robot Base";
                robot.base.tracked(1:framecount)=true;
                robot.base.x=data.Xb;  robot.base.y=data.Yb;  robot.base.z=data.Zb;
                robot.base.qx=data.qxb; robot.base.qy=data.qyb; robot.base.qz=data.qzb; robot.base.qw=data.qwb;
                robot.base.ax=data.rxb;  robot.base.ay=data.ryb;  robot.base.az=data.rzb;  
                
            case (2)
                %second object is Joint 1
                data.Properties.VariableNames(j:j+7)={'Joint1','Xj1','Yj1','Zj1','qxj1','qyj1','qzj1','qwj1'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxj1','ryj1','rzj1'}; j=j+3;
                robot.j1.name="Joint 1";
                robot.j1.tracked(1:framecount)=true;
                robot.j1.x=data.Xj1;  robot.j1.y=data.Yj1;  robot.j1.z=data.Zj1;
                robot.j1.qx=data.qxj1; robot.j1.qy=data.qyj1; robot.j1.qz=data.qzj1; robot.j1.qw=data.qwj1;
                robot.j1.ax=data.rxj1;  robot.j1.ay=data.ryj1;  robot.j1.az=data.rzj1;  
                
            case(3)
                %Third object is Joint 3
                data.Properties.VariableNames(j:j+7)={'Joint3','Xj3','Yj3','Zj3','qxj3','qyj3','qzj3','qwj3'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxj3','ryj3','rzj3'}; j=j+3;
                robot.j3.name="Joint 3";
                robot.j3.tracked(1:framecount)=true;
                robot.j3.x=data.Xj3;  robot.j3.y=data.Yj3;  robot.j3.z=data.Zj3;
                robot.j3.qx=data.qxj3; robot.j3.qy=data.qyj3; robot.j3.qz=data.qzj3; robot.j3.qw=data.qwj3;
                robot.j3.ax=data.rxj3;  robot.j3.ay=data.ryj3;  robot.j3.az=data.rzj3; 
                
            case(4)
                %fourth object is Joint 4
                data.Properties.VariableNames(j:j+7)={'Joint4','Xj4','Yj4','Zj4','qxj4','qyj4','qzj4','qwj4'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxj4','ryj4','rzj4'}; j=j+3;
                robot.j4.name="Joint 3";
                robot.j4.tracked(1:framecount)=true;
                robot.j4.x=data.Xj4;  robot.j4.y=data.Yj4;  robot.j4.z=data.Zj4;
                robot.j4.qx=data.qxj4; robot.j4.qy=data.qyj4; robot.j4.qz=data.qzj4; robot.j4.qw=data.qwj4;
                robot.j4.ax=data.rxj4;  robot.j4.ay=data.ryj4;  robot.j4.az=data.rzj4; 
                
            case (5)
                %fith object is Joint 5
                data.Properties.VariableNames(j:j+7)={'Joint5','Xj5','Yj5','Zj5','qxj5','qyj5','qzj5','qwj5'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxj5','ryj5','rzj5'}; j=j+3;
                robot.j5.name="Joint 3";
                robot.j5.tracked(1:framecount)=true;
                robot.j5.x=data.Xj5;  robot.j5.y=data.Yj5;  robot.j5.z=data.Zj5;
                robot.j5.qx=data.qxj5; robot.j5.qy=data.qyj5; robot.j5.qz=data.qzj5; robot.j5.qw=data.qwj5;
                robot.j5.ax=data.rxj5;  robot.j5.ay=data.ryj5;  robot.j5.az=data.rzj5; 
                
            case (6)
                %sixth object is Joint 6
                data.Properties.VariableNames(j:j+7)={'Joint6','Xj6','Yj6','Zj6','qxj6','qyj6','qzj6','qwj6'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxj6','ryj6','rzj6'}; j=j+3;
                robot.j6.name="Joint 3";
                robot.j6.tracked(1:framecount)=true;
                robot.j6.x=data.Xj6;  robot.j6.y=data.Yj6;  robot.j6.z=data.Zj6;
                robot.j6.qx=data.qxj6; robot.j6.qy=data.qyj6; robot.j6.qz=data.qzj6; robot.j6.qw=data.qwj6;
                robot.j6.ax=data.rxj6;  robot.j6.ay=data.ryj6;  robot.j6.az=data.rzj6; 
                
            case (7)
                %Seventh object is End Effector
                data.Properties.VariableNames(j:j+7)={'EndEffector','Xe','Ye','Ze','qxe','qye','qze','qwe'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rxe','rye','rze'}; j=j+3;
                robot.end.name="Joint 3";
                robot.end.tracked(1:framecount)=true;
                robot.end.x=data.Xe;  robot.end.y=data.Ye;  robot.end.z=data.Ze;
                robot.end.qx=data.qxe; robot.end.qy=data.qye; robot.end.qz=data.qze; robot.end.qw=data.qwe;
                robot.end.ax=data.rxe;  robot.end.ay=data.rye;  robot.end.az=data.rze; 
                
            otherwise
                %all other objects are containers
                robot.containers=robot.containers+1;
                data.Properties.VariableNames(j:j+3)={sprintf('Contaner%d',rbID),sprintf('X%d',rbID),sprintf('Y%d',rbID),sprintf('Z%d',rbID)}; j=j+4;
                data.Properties.VariableNames(j:j+3)={sprintf('qxc%d',rbID),sprintf('qyc%d',rbID),sprintf('qzc%d',rbID),sprintf('qwc%d',rbID)}; j=j+4;
                data.Properties.VariableNames(j:j+2)={sprintf('rx%d',rbID),sprintf('ry%d',rbID),sprintf('rz%d',rbID)}; j=j+3;
                robot.(sprintf('c%i',rbID)).name=sprintf('Contaner %d',rbID);
                robot.(sprintf('c%i',rbID)).tracked(1:framecount)=true;
                robot.(sprintf('c%i',rbID)).x=data.(sprintf('X%d',rbID));  robot.(sprintf('c%i',rbID)).y=data.(sprintf('Y%d',rbID));  robot.(sprintf('c%i',rbID)).z=data.(sprintf('Z%d',rbID));
                robot.(sprintf('c%i',rbID)).qx=data.(sprintf('qxc%d',rbID)); robot.(sprintf('c%i',rbID)).qy=data.(sprintf('qyc%d',rbID)); robot.(sprintf('c%i',rbID)).qz=data.(sprintf('qzc%d',rbID)); robot.(sprintf('c%i',rbID)).qw=data.(sprintf('qwc%d',rbID));
                robot.(sprintf('c%i',rbID)).ax=data.(sprintf('rx%d',rbID));  robot.(sprintf('c%i',rbID)).ay=data.(sprintf('ry%d',rbID));  robot.(sprintf('c%i',rbID)).az=data.(sprintf('rz%d',rbID)); 
        end
        
    end
    
    if rigidbodycount~=7+robot.containers
        disp('Not all robot joints found.')
    end
    save(['saved files\',filename],'robot');
end



