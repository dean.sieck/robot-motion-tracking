

function CreateJointAnglePlots
	% sets up two plots for viewing rigid body data, position and rotation
	global ja JA
	% making animated lines global so they can be accessed in the
	% callback functions
    global rx ry rz
    global names
    
    rx=animatedline();
    ry=animatedline();
    rz=animatedline();

    JA=figure(1);
    JA.Name='Joint Angles';
    JA.Position=[1921 177 1280 948];
    for i=1:6
        %plot an animated line for Rotation
        ja(i) = subplot( 2,3,i);
        title( names(i+1) );
        xlabel( 'Frame' )
        ylabel( 'Position (m)' )

        rx(i) = animatedline;
        rx(i).MaximumNumPoints = 1000; 
        rx(i).Marker = '.';
        rx(i).LineWidth = 0.5;
        rx(i).Color = [ 1 0 0 ];

        ry(i) = animatedline;
        ry(i).MaximumNumPoints = 1000;
        ry(i).LineWidth = 0.5;
        ry(i).Color = [ 0 1 0 ];
        ry(i).Marker = '.';

        rz(i) = animatedline;
        rz(i).MaximumNumPoints = 1000;
        rz(i).LineWidth = 0.5;
        rz(i).Color = [ 0 0 1 ];
        rz(i).Marker = '.';
    end
    legend({'X','Y','Z'},'Position',[0.4512 0.0218 0.1352 0.0226],'Orientation','horizontal')


end
