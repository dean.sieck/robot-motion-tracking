function MotiveFileRobotAndContainers
%MotiveFile takes the name of a .csv file produced by matlab and transforms
%it into two files for the frame data and the rigidbody data.

%Returns matrices of x,y,z location of robot head and containers with
%respect to robot base and the number of containers, frames and the table with data from frames 



% frame: Details frame of exported data in the following format:
%    -- Frame Index (integer)
%    -- Time Stamp (double) in seconds
%    -- Rigid Body Count (integer) number of rigid bodies tracked in current frame
%    -- Rigid Body ID, Position, Quaternion Orientation, and Euler Angle Orientation >for each tracked rigid body< (ID, x,y,z, qx, qy, qz, qw, yaw, pitch, roll)
%    -- Marker Count (integer) Count of all visible markers in frame
%    -- Marker Detail >for each reconstructed 3D marker< (x,y,z,id,name)

% rigidbody: Extended information for current frame:
%    -- Frame Index (integer)
%    -- Time Stamp (double) in seconds
%    -- Name (string)
%    -- ID   (integer)
%    -- Frames Since Last Tracked (integer). 0=currently tracked
%    -- Marker Count (integer)
%    -- Rigid Body Marker Location and ID >for each marker in rigid body< (x,y,z,id)
%    -- Point Cloud Marker corresponding to each rigid body marker (x,y,z). (NAN,NAN,NAN) used for missing Point Cloud Markers.
%    -- Marker Tracked (integer) for each marker. 1=track, 0=untracked.
%    -- Marker Quality (float) for each marker. 1.0=Highest Quality, 0.0=Lowest Quality
%    -- Mean Error (in mm/marker)


global filename data time base joint1 joint3 joint4 joint5 joint6 endl enda containers numberofcontainers frames    
%% open the files
      
    framename=strcat(filename,'_frame','.txt');
    rigidbodyname=strcat(filename,'_rigidbody','.txt');
    filename=strcat(filename,'.csv');

    if exist(framename, 'file')
      delete(framename)
      delete(rigidbodyname)
    end

    fileID=fopen(filename,'r');
    frameID=fopen(framename,'a');
    rigidbodyID=fopen(rigidbodyname,'a');

    ln=0;


    %% put data in respective files
    for i = 1:41
        fgetl(fileID);
    end

    a=fgetl(fileID); 
    version=sscanf(a,'info,version,%f');
    a=fgetl(fileID); 
    frames=sscanf(a,'info,framecount,%d');
    a=fgetl(fileID); 
    rigidbodycount=sscanf(a,'info,rigidbodycount,%d');
    fgetl(fileID);
    


    for i=1:rigidbodycount
        a=fgets(fileID);
    end

    while (a~=-1)
        ln=ln+1;
        if mod(ln,rigidbodycount+1)==1
            fprintf(frameID,'%s',a);
        else
            fprintf(rigidbodyID,'%s',a);
        end
        a=fgets(fileID);
    end

    fclose('all');

    
    %% Make tables
    % and Re-orient to make robot base the origin 
    data=readtable(framename);
    data.Properties.VariableNames(2:4)={'Frame','Time','RigidBodyCount'};
    rigidbodycount=data.RigidBodyCount(1);
    numberofcontainers=0;
    containers=zeros(frames,3);
    time=data.Time;
    pause(3)
    j=5;
    for i=1:rigidbodycount
        rbID=data{1,j};
        switch rbID
            case (1)
                %first object is robot base
                data.Properties.VariableNames(j:j+3)={'RobotBase','Xb','Yb','Zb'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXb','rYb','rZb'}; j=j+3;
                base=[data.Xb,data.Yb,data.Zb];
            case (2)
                %second object is Joint 1
                data.Properties.VariableNames(j:j+3)={'Joint1','Xj1','Yj1','Zj1'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXj1','rYj1','rZj1'}; j=j+3;
                joint1=[data.Xj1,data.Yj1,data.Zj1];
            case(3)
                %Third object is Joint 3
                data.Properties.VariableNames(j:j+3)={'Joint3','Xj3','Yj3','Zj3'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXj3','rYj3','rZj3'}; j=j+3;
                joint3=[data.Xj3,data.Yj3,data.Zj3];
            case(4)
                %fourth object is Joint 4
                data.Properties.VariableNames(j:j+3)={'Joint4','Xj4','Yj4','Zj4'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXj4','rYj4','rZj4'}; j=j+3;
                joint4=[data.Xj4,data.Yj4,data.Zj4];
            case (5)
                %fith object is Joint 5
                data.Properties.VariableNames(j:j+3)={'Joint5','Xj5','Yj5','Zj5'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXj5','rYj5','rZj5'}; j=j+3;
                joint5=[data.Xj5,data.Yj5,data.Zj5];
            case (6)
                %sixth object is Joint 6
                data.Properties.VariableNames(j:j+3)={'Joint6','Xj6','Yj6','Zj6'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXj6','rYj6','rZj6'}; j=j+3;
                joint6=[data.Xj6,data.Yj6,data.Zj6];
            case (7)
                %Seventh object is End Effector
                data.Properties.VariableNames(j:j+3)={'EndEffector','Xe','Ye','Ze'}; j=j+8;
                data.Properties.VariableNames(j:j+2)={'rXe','rYe','rZe'}; j=j+3;
                endl=[data.Xe,data.Ye,data.Ze];
            otherwise
                %all other objects are containers
                numberofcontainers=numberofcontainers+1;
                data.Properties.VariableNames(j:j+3)={sprintf('Contaner%d',rbID),sprintf('X%d',rbID),sprintf('Y%d',rbID),sprintf('Z%d',rbID)}; j=j+8;
                data.Properties.VariableNames(j:j+2)={sprintf('rX%d',rbID),sprintf('rY%d',rbID),sprintf('rZ%d',rbID)}; j=j+3;
                containers(:,:,numberofcontainers)=[eval(['data.X' num2str(rbID)]),eval(['data.Y' num2str(rbID)]),eval(['data.Z' num2str(rbID)])];
                
        end
        
    end
    
    if rigidbodycount~=7+numberofcontainers
            disp('Not all robot joints found.')
    end
        
end



