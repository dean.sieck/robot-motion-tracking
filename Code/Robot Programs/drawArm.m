function [ output_args ] = drawArm( J1, J2, J3, J4, J5, J6 )
    
    J1Temp = J1 + 90;
    J2Temp = J2 + 180;
    wristD = 0.09960141; %wristD = 0.0823;
    wristSideD = .12670283;
    toolD = 0.170;
    L1 = .42499948; %L1 = 0.4257;
    L2 = 0.39214349;  %L2 = 0.39155;
    baseSideD = 0.13779926- 0.0059539; %baseSideD = 0.0997;
    baseHeight = 0.1625;
    
    sP = [cosd(J1Temp)*baseSideD, sind(J1Temp)*baseSideD, baseHeight];
    
    hold on
    plot3([0,0],[0,0], [0,baseHeight], 'linewidth', 3, 'color', 'r')
    
    plot3([0,cosd(J1Temp)*baseSideD],[0,sind(J1Temp)*baseSideD], [baseHeight,baseHeight], 'b', 'linewidth', 2.5)
    
    plot3([sP(1), sP(1)+L1*cosd(J2Temp)*cosd(J1Temp+90)],[sP(2), sP(2)+L1*cosd(J2Temp)*sind(J1Temp+90)],[sP(3), L1*sind(J2Temp) + sP(3)], 'b', 'linewidth', 2)
    
    
    
    elbow = [L1*cosd(J2Temp)*cosd(J1Temp+90), L1*cosd(J2Temp)*sind(J1Temp+90), L1*sind(J2Temp) + sP(3)];
    wrist = [L2*cosd(J3+J2Temp)*cosd(J1Temp+90), L2*cosd(J3+J2Temp)*sind(J1Temp+90), L2*sind(J3+J2Temp)];
    
    plot3([sP(1)+L1*cosd(J2Temp)*cosd(J1Temp+90), elbow(1)],[sP(2)+L1*cosd(J2Temp)*sind(J1Temp+90), elbow(2)],[L1*sind(J2Temp) + sP(3), elbow(3)], 'b', 'linewidth', 1.5)
    
    plot3([elbow(1), elbow(1) + wrist(1)],[elbow(2), elbow(2) + wrist(2)],[elbow(3), elbow(3) + wrist(3)],'b')
    plot3([elbow(1) + wrist(1), elbow(1) + wrist(1) + wristSideD*cosd(J1Temp)],[ elbow(2) + wrist(2), elbow(2) + wrist(2) + wristSideD*sind(J1Temp)],[elbow(3) + wrist(3), elbow(3) + wrist(3)],'g', 'linewidth', 1.2)
    
    %plot3([shapeLoc(1), cameraWrist(1)],[shapeLoc(2), cameraWrist(2)],[shapeLoc(3), cameraWrist(3)], 'b', 'linewidth', 1)
    if J4 >= -90
    
        plot3([elbow(1) + wrist(1) + wristSideD*cosd(J1Temp), elbow(1) + wrist(1) + wristSideD*cosd(J1Temp)],[elbow(2) + wrist(2) + wristSideD*sind(J1Temp), elbow(2) + wrist(2) + wristSideD*sind(J1Temp)],[elbow(3) + wrist(3), elbow(3) + wrist(3)+0.0997], 'c')
        plot3([elbow(1) + wrist(1) + wristSideD*cosd(J1Temp), elbow(1) + wrist(1) + wristSideD*cosd(J1Temp)+ 0.099601*cosd(J1Temp + J5)],[elbow(2) + wrist(2) + wristSideD*sind(J1Temp), elbow(2) + wrist(2) + wristSideD*sind(J1Temp)+ 0.099601*sind(J1Temp + J5)],[elbow(3) + wrist(3)+0.0997, elbow(3) + wrist(3)+0.0997], 'c')

    else
        plot3([elbow(1) + wrist(1) + wristSideD*cosd(J1Temp), elbow(1) + wrist(1) + wristSideD*cosd(J1Temp)],[elbow(2) + wrist(2) + wristSideD*sind(J1Temp), elbow(2) + wrist(2) + wristSideD*sind(J1Temp)],[elbow(3) + wrist(3), elbow(3) + wrist(3)-0.0997], 'c')
        plot3([elbow(1) + wrist(1) + wristSideD*cosd(J1Temp + J5), elbow(1) + wrist(1) + wristSideD*cosd(J1Temp)+ 0.099601*cosd(J1Temp + J5)],[elbow(2) + wrist(2) + wristSideD*sind(J1Temp), elbow(2) + wrist(2) + wristSideD*sind(J1Temp)+ 0.099601*sind(J1Temp + J5)],[elbow(3) + wrist(3)-0.0997, elbow(3) + wrist(3)-0.0997], 'c')
    
    end
    
    

    %fullArm = patch(stl, 'FaceColor', [0.8, 0.8, 1], 'EdgeColor', 'none', 'FaceLighting', 'gouraud', 'AmbientStrength', 0.15);
    %rotate(fullArm, [0 1 0], 45)

    
    %view([-135, 35])
end

