function [ ] = moveToolTimeJ(x, y, z, rx, ry, rz, t)
%moveToLocation This function moves the arm to a location defined by the
%parameters x, y, z (in meters) and rx, ry, rz (in radians).
global robot    
    A = [x, y, z, rx, ry, rz, t];
    fprintf(robot, 'stopj(0.3)\n');
    pause(0.5)
    fprintf(robot, 'movej(p[%.3f, %.3f, %.3f, %.4f, %.4f, %.4f], a=.1,v=.1,t=%.3f)\n',A);
end

