

function CreateQuaternionPlots
	% sets up two plots for viewing rigid body data, position and rotation
	global ja JA
	% making animated lines global so they can be accessed in the
	% callback functions
    global x y z dist dist2
    global names
    
    x=animatedline();
    y=animatedline();
    z=animatedline();
    dist=animatedline();

    JA=figure(1);
    JA.Name='Distance Between Joints';
    JA.Position=[1921 177 1280 948];
    
    for i=1:6
        %plot an animated line for Rotation
        ja(i) = subplot( 2,3,i);
        title(sprintf('%s to %s', names(i+1), names(i) ));
        xlabel( 'Time (s)' )
        ylabel( 'distance (m)' )

        x(i) = animatedline;
        x(i).MaximumNumPoints = 1000; 
        x(i).Marker = '.';
        x(i).LineWidth = 0.5;
        x(i).Color = [ 1 0 0 ];

        y(i) = animatedline;
        y(i).MaximumNumPoints = 1000;
        y(i).LineWidth = 0.5;
        y(i).Color = [ 0 1 0 ];
        y(i).Marker = '.';

        z(i) = animatedline;
        z(i).MaximumNumPoints = 1000;
        z(i).LineWidth = 0.5;
        z(i).Color = [ 0 0 1 ];
        z(i).Marker = '.';
        
        dist(i) = animatedline;
        dist(i).MaximumNumPoints = 1000;
        dist(i).LineWidth = 1;
        dist(i).Color = 'k';
        dist(i).Marker = '.';
        
    end
    legend({'X','Y','Z','Distance'},'Position',[0.4512 0.0218 0.1352 0.0226],'Orientation','horizontal')


end
