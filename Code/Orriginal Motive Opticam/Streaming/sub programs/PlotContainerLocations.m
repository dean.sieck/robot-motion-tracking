function PlotContainerLocations

global names rbnum ID 
global base endl containers sketch
containers=animatedline();
rbnum=length(ID);

%create an image of locations
f=figure(1);
f.Position=[1921 177 1280 948];
title( 'Container Locations' );
axis([-1,1,-1,1,-1,1])

base =animatedline(0,0,0);
base.MaximumNumPoints = 1; 
base.Marker = '+';
base.LineWidth = 1;
base.Color = 'k';

endl = animatedline(0,0,0);
endl.MaximumNumPoints = 1; 
endl.Marker = '*';
endl.LineWidth = 1;
endl.Color = 'r';

sketch=animatedline(1:7,1:7,1:7);
sketch.MaximumNumPoints = 7; 
sketch.LineWidth = 1;
sketch.Color = 'r';

for i=1:rbnum-7
containers(i)=animatedline(0,0,0);
containers(i).MaximumNumPoints = 1; 
containers(i).Marker = 'o';
containers(i).LineWidth = 1;
containers(i).Color = 'b';
containers(i).MarkerFaceColor = 'c';

end


xlabel( 'X Direction (m)' );
ylabel( 'Y Direction (m)' );
zlabel( 'Z Direction (m)');

end

