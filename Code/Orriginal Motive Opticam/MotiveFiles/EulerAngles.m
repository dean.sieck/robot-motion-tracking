%function EulerAngles
%Convert EulerAngles to global Angles
%xyz current frame euler angles given by motive
% global data enda filename
% filename='Test';
% MotiveFileRobotAndContainers
clc

calc=mean(enda);
dx=mean(data.rXe); dy=mean(data.rYe); dz=mean(data.rZe);
Rx=[1,0,0;0,cos(dx),-sin(dx);0,sin(dx),cos(dx)];
Ry=[cos(dy),0,sin(dy);0,1,0;-sin(dy),0,cos(dy)];
Rz=[cos(dz),-sin(dz),0;sin(dz),cos(dz),0;0,0,1];
R=Rx*Ry*Rz;
Ve=[0,0,1]';
euler=(R*Ve)';

magcalc=sqrt(calc(1)^2+calc(2)^2+calc(3)^2);
mageuler=sqrt(euler(1)^2+euler(2)^2+euler(3)^2);

calc=calc*1/magcalc
euler=euler*1/mageuler

hold on
plot3(calc(1),calc(2),calc(3))
plot3([0,euler(1)],[0,euler(2)],[0,euler(3)])
hold off
%end

