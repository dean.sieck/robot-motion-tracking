
function containerlocations( ~ , evnt )

global endl base containers sketch
global ID names rbnum

for i=[1,7:rbnum]
    % check the rigid body position
    if ID(i)==0
        clc
        fprintf('ERROR: %s not defined with ID = %d\n',names(i),i)
        return
    end
end

%add sketch points
for i=1:7
    rx(i) = double( evnt.data.RigidBodies( ID(i) ).x );
    ry(i) = -double( evnt.data.RigidBodies( ID(i) ).z );
    rz(i) = double( evnt.data.RigidBodies( ID(i) ).y );
end
base.addpoints(rx(1),ry(1),rz(1));
endl.addpoints(rx(7),ry(7),rz(7));
sketch.addpoints(rx,ry,rz)

%find rotation matrix for base
qx=double( evnt.data.RigidBodies( ID(1) ).qx );
qy=-double( evnt.data.RigidBodies( ID(1) ).qz );
qz=double( evnt.data.RigidBodies( ID(1) ).qy );
qw=double( evnt.data.RigidBodies( ID(1) ).qw );
Ab=[2*(qw^2+qx^2)-1,  2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy),  rx(1);...
    2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,    2*(qy*qz-qw*qx),  ry(1);...
    2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1,  rz(1);...
    0,                0,                  0,                1];


%find rotation matrix for end effector
qx=double( evnt.data.RigidBodies( ID(7) ).qx );
qy=-double( evnt.data.RigidBodies( ID(7) ).qz );
qz=double( evnt.data.RigidBodies( ID(7) ).qy );
qw=double( evnt.data.RigidBodies( ID(7) ).qw );
Ae=[2*(qw^2+qx^2)-1,  2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy),  rx(7);...
    2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,    2*(qy*qz-qw*qx),  ry(7);...
    2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1,  rz(7);...
    0,                0,                  0,                1];


%add container points
for i=1:rbnum-7
    cx = double( evnt.data.RigidBodies( ID(i+7) ).x );
    cy = -double( evnt.data.RigidBodies( ID(i+7) ).z );
    cz = double( evnt.data.RigidBodies( ID(i+7) ).y );
    po=[cx,cy,cz,1]';
    qx=double( evnt.data.RigidBodies( ID(i+7) ).qx );
    qy=-double( evnt.data.RigidBodies( ID(i+7) ).qz );
    qz=double( evnt.data.RigidBodies( ID(i+7) ).qy );
    qw=double( evnt.data.RigidBodies( ID(i+7) ).qw );
    Ac=[2*(qw^2+qx^2)-1,  2*(qx*qy-qw*qz),    2*(qx*qz+qw*qy),  rx(7);...
        2*(qx*qy+qw*qz),  2*(qw^2+qy^2)-1,    2*(qy*qz-qw*qx),  ry(7);...
        2*(qx*qz-qw*qy),  2*(qy*qz+qw*qx),    2*(qw^2+qz^2)-1,  rz(7);...
        0,                0,                  0,                1];
    
    containers(i).addpoints(cx,cy,cz);

    %coordinates of container relative to base and end
    cb(:,i)=Ab\po;
    ce(:,i)=Ae\po;
    
    %angle of container relative to end
    R=(Ab(1:3,1:3)\Ae(1:3,1:3))\(Ab(1:3,1:3)\Ac(1:3,1:3));
    q=.5*[sign(R(3,2)-R(2,3)).*sqrt(max(R(1,1)-R(2,2)-R(3,3)+1,0)),...
        sign(R(1,3)-R(3,1)).*sqrt(max(R(2,2)-R(3,3)-R(1,1)+1,0)),...
        sign(R(2,1)-R(1,2)).*sqrt(max(R(3,3)-R(1,1)-R(2,2)+1,0))];
    q(4)=.5*min(sqrt(R(1,1)+R(2,2)+R(3,3)+1),2);
    Angle(i,1)=round(acos(q(4))*2*180/pi);
    
    
end
%distance between end effector and base
eb=Ab\Ae(:,4);

range=max([1,max(cb(1:3,:))]);
axis([rx(1)-range,rx(1)+range,ry(1)-range,ry(1)+range,rz(1)-range,rz(1)+range])
drawnow

%list of container numbers for tables
Container=names(8:rbnum)';

%distance from end to containers
x=round(ce(1,:)'*1000);
y=round(ce(2,:)'*1000);
z=round(ce(3,:)'*1000);
Horizontal=round(sqrt(x.^2+y.^2));
Total=round(sqrt(x.^2+y.^2+z.^2));
end2containers=table(Container,Horizontal,Total,x,y,z,Angle);


%distance from base to containers
x=round(cb(1,:)'*1000);
y=round(cb(2,:)'*1000);
z=round(cb(3,:)'*1000);
Horizontal=round(sqrt(x.^2+y.^2));
Total=round(sqrt(x.^2+y.^2+z.^2));
base2containers=table(Container,Horizontal,Total,x,y,z);



clc
fprintf('Distance Between End Effector and Base: (%0.3f,%0.3f,%0.3f,)m\n',eb(1),eb(2),eb(3))
fprintf('Distance Between End Effector and Base: %0.3f m\n\n',sqrt(eb(1)^2+eb(2)^2+eb(3)^2))

disp('Distance From End Effector To Cointainers (mm)')
disp(end2containers);

fprintf('\nDistance From Base To Cointainers (mm)\n')
disp(base2containers);

end  % eventcallback1
