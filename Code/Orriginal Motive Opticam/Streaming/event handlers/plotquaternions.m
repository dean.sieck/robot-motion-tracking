
 function plotquaternions( ~ , evnt )
 % The event callback function executs each time a frame of mocap data is delivered.
 % to Matlab. Matlab will lag if the data rate from the Host is too high.
 % A simple animated line graphs the x, y, z position of the first rigid body on the Host.
 
 
 % Note - This callback uses the gobal variables px, py, pz from the NatNetEventHandlerSample.
 global x y z dist 
 global ja JA
 global ID names
 
 % local variables
 persistent frame1 lastframe1
 scope = .5;
 
 tracked=ones(1,7);
 for i=1:7  % collect data for angles
    if ID(i)==0
        clc
        fprintf('ERROR: %s not defined with ID = %d\n',names(i),i)
        return
    end
    
    if evnt.data.RigidBodies( ID(i) ).MeanError==0
        tracked(i)=false;
    end
   
    rb = evnt.data.RigidBodies( ID(i) );
     qx(i) = double(rb.qx);
     qy(i) = -double(rb.qz);
     qz(i) = double(rb.qy);
     qw(i) = double(rb.qw);
     px(i) = double(rb.x);
     py(i) = -double(rb.z);
     pz(i) = double(rb.y);
     
 end
%convert raw data to joint data

%calculate joint angles
[r]=StreamingJointAngle(qx,qy,qz,qw,px,py,pz,tracked);

for i=2:7
    %get distance information for plotting
    rbc=evnt.data.RigidBodies(ID(i));
    rbl=evnt.data.RigidBodies(ID(i-1));
     
     xc=double(rbc.x);
     yc=-double(rbc.z);
     zc=double(rbc.y);
     
     xl=double(rbl.x);
     yl=-double(rbl.z);
     zl=double(rbl.y);
     
     dx(i)=xl-xc;
     dy(i)=yl-yc;
     dz(i)=zl-zc;
     d(i)=sqrt(dx(i)^2+dy(i)^2+dz(i)^2);
     
    %get frame number
	frame1 = double( evnt.data.iFrame );
	if ~isempty( frame1 ) && ~isempty( lastframe1 )
		if frame1 < lastframe1
			x(i-1).clearpoints;
			y(i-1).clearpoints;
			z(i-1).clearpoints;
            dist(i-1).clearpoints;
		end
    end
    
    time=toc;
    %if its tracked
    if  evnt.data.RigidBodies( ID(i) ).MeanError~=0
        % Fill the animated line's queue with the rb position
        x(i-1).addpoints( time , r(1,i-1) );
        y(i-1).addpoints( time , r(2,i-1) );
        z(i-1).addpoints( time , r(3,i-1) );
        dist(i-1).addpoints( time , d(i));
    end

	
	% set the figure and subplot to graph the data
	set(JA , 'CurrentAxes' , ja(i-1))

	% Dynamically move the axis of the graph
	axis( [ time-10 ,  time , -scope , scope ] );
	
	% Draw the data to a figure
	drawnow
    
	% Update lastframe
	lastframe1 = frame1;
    
end


    
end  % eventcallback1
