function [r]=StreamingJointAngle(qx,qy,qz,qw,x,y,z,tracked)
global names
persistent P1 P2 runs
runs=runs+1;

% Calculate Joint Angles

%.6find rotation matrix from orign (save seperatly for possible future use)
for i=1:7
    R0(:,:,i)=[2*(qw(i)^2+qx(i)^2)-1,     2*(qx(i)*qy(i)-qw(i)*qz(i)),    2*(qx(i)*qz(i)+qw(i)*qy(i));...
        2*(qx(i)*qy(i)+qw(i)*qz(i)),  2*(qw(i)^2+qy(i)^2)-1,          2*(qy(i)*qz(i)-qw(i)*qx(i));...
        2*(qx(i)*qz(i)-qw(i)*qy(i)),  2*(qy(i)*qz(i)+qw(i)*qx(i)),    2*(qw(i)^2+qz(i)^2)-1];
end

%set each rotation matrix according to robot base (save seperatly for possible future use)
R(:,:,1)=R0(:,:,1);
for i=2:7
    R(:,:,i)=(R0(:,:,1))\R0(:,:,i); %matlab recomended using A\b instead of inv(A)*b
end

%set each rotation matrix according to previous joint
for i=7:-1:3
    R(:,:,i)=(R(:,:,i-1))\R(:,:,i); 
end


%local quaternian qx,qy,qz
q(:,:)=.5*[sign(R(3,2,:)-R(2,3,:)).*sqrt(max(R(1,1,:)-R(2,2,:)-R(3,3,:)+1,0)),...
            sign(R(1,3,:)-R(3,1,:)).*sqrt(max(R(2,2,:)-R(3,3,:)-R(1,1,:)+1,0)),...
            sign(R(2,1,:)-R(1,2,:)).*sqrt(max(R(3,3,:)-R(1,1,:)-R(2,2,:)+1,0))];
%quaternian qw
q(4,1:7)=.5*min(sqrt(R(1,1,:)+R(2,2,:)+R(3,3,:)+1),2);
%joint angle
angle=acos(q(4,:)).*2*180/pi;


%adjust angles
%base
if q(3,1)<0
    angle(1)=-angle(1);
end
%joint 1
if q(3,2)<0
    angle(2)=-angle(2);
end
%joint 3
if q(2,3)>0
    angle(3)=-angle(3);
end
%joint 4
if q(2,4)>0
    angle(4)=-angle(4);
end
%joint 5
if q(2,5)>0
    angle(5)=-angle(5);
end
%joint 6
if q(3,6)>0
    angle(6)=-angle(6);
end
%end effector
if q(2,7)>0
    angle(7)=-angle(7);
end


v0(:,1,:)=[x;y;z];
A=[R0,v0;zeros(1,4,7)];
A(4,4,:)=1;
p0(:,1,:)=[x;y;z];
p0(4,:,:)=1;
for i=2:7
  p1(:,i)=inv(A(:,:,i))*p0(:,:,i-1);  
  p2(:,i-1)=inv(A(:,:,i-1))*p0(:,:,i);  
end
  p2(:,7)=zeros(1,4);

  r=p1(1:3,2:7);
%   r=p2(1:3,1:6);

if tracked
    if isempty(P1)
        P1=p1;
        P2=p2;
        runs=0;
    else
        %use P1 & P2 to recalibrate vectors between joint in MotiveRecording Program
        format short
        P1(1,:)=(P1(1,:)*runs+p1(1,:))./(runs+1);
        P1(2,:)=(P1(2,:)*runs+p1(2,:))./(runs+1);
        P1(3,:)=(P1(3,:)*runs+p1(3,:))./(runs+1);
        
        P2(1,:)=(P2(1,:)*runs+p2(1,:))./(runs+1);
        P2(2,:)=(P2(2,:)*runs+p2(2,:))./(runs+1);
        P2(3,:)=(P2(3,:)*runs+p2(3,:))./(runs+1);
                
    end
end

P2(:,2:3)=zeros(4,2);

Angle=angle';
Joint=names(1:7)';
format bank
clc
disp(table(Joint,Angle))
format
P1
P2
    
end




