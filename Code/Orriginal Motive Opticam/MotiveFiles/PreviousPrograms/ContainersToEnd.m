function ContainersToEnd
global data time base joint1 joint3 joint4 joint5 joint6 endl enda containers numberofcontainers frames    
%Plot container location relative to end
        for i=1:numberofcontainers;
           %Distance from head to container
           dhc(:,1:3,i)=containers(:,1:3,i)-endl;
           dhc(:,4,i)=sqrt(dhc(:,1,i).^2+dhc(:,2,i).^2);                   %xy distance 
           dhc(:,5,i)=sqrt(dhc(:,1,i).^2+dhc(:,2,i).^2+dhc(:,3,i).^2);      %xyz distance

           %Distance from base to container
           containers(:,4,i)=sqrt(containers(:,1,i).^2+containers(:,2,i).^2);                      %xy distance
           containers(:,5,i)=sqrt(containers(:,1,i).^2+containers(:,2,i).^2+containers(:,3,i).^2);          %xyz distance
        end

        %Distance from base to head
        bh(:,1)=sqrt(endl(:,1).^2+endl(:,2).^2);                      %xy distance
        bh(:,2)=sqrt(endl(:,1).^2+endl(:,2).^2+endl(:,3).^2);          %xyz distance


        % Plot things

       for i=1:numberofcontainers
            figure('Name', ['Distance From Robot Head to Container', num2str(i)]);

            subplot(2,3,4)
            plot(time,dhc(:,1,i))
            title('X Direction')
            xlim([0,max(time)])
            xlabel('Time (s)')
            ylabel('Location (m)')


            subplot(2,3,5)
            plot(time,dhc(:,2,i))
            title('Y Direction')
            xlim([0,max(time)])
            xlabel('Time (s)')
            ylabel('Location (m)')

            subplot(2,3,6)
            plot(time,dhc(:,3,i))
            title('Z Direction')
            xlim([0,max(time)])
            xlabel('Time (s)')
            ylabel('Location (m)')

            subplot(2,3,1:2)
            plot(time,dhc(:,4,i))
            title('Horizontal Distance')
            xlim([0,max(time)])
            xlabel('Time (s)')
            ylabel('Distance (m)')

            subplot(2,3,3)
            plot(time,dhc(:,5,i))
            title('Total Distance')
            xlim([0,max(time)])
            xlabel('Time (s)')
            ylabel('Distance (m)')
       end
end

